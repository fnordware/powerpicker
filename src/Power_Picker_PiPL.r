#include "AEConfig.h"

#include "AE_EffectVers.h"

#ifndef AE_OS_WIN
	#include "AE_General.r"
	#include "Types.r"
	#include "SysTypes.r"
#endif


#define USE_HSV 0

resource 'PiPL' (16000) {
	{	/* array properties: 12 elements */
		/* [1] */
		Kind {
			AEEffect
		},
		/* [2] */
		Name {
#if USE_HSV
			"Power Picker HSV"
#else
			"Power Picker"
#endif
		},
		/* [3] */
		Category {
			"fnord"
		},
		
#ifdef AE_OS_WIN
	#ifdef AE_PROC_INTELx64
		CodeWin64X86 {"main"},
	#else
		CodeWin32X86 {"main"},
	#endif	
#else
	#ifdef AE_PROC_INTELx64
		CodeMacIntel64 {"main"},
	#else
		CodeMachOPowerPC {"main"},
		CodeMacIntel32 {"main"},
	#endif
#endif		/* [6] */
		AE_PiPL_Version {
			2,
			0
		},
		/* [7] */
		AE_Effect_Spec_Version {
			PF_PLUG_IN_VERSION,
			PF_PLUG_IN_SUBVERS
		},
		/* [8] */
		AE_Effect_Version {
			591360 /* 2.0 */
		},
		/* [9] */
		AE_Effect_Info_Flags {
			0
		},
		/* [10] */
		AE_Effect_Global_OutFlags {
			33588289
		},
		AE_Effect_Global_OutFlags_2 {
			0
		},
		/* [11] */
		AE_Effect_Match_Name {
#if USE_HSV
			"fnord Power Picker HSV"
#else
			"fnord Power Picker"
#endif
		},
		/* [12] */
		AE_Reserved_Info {
			0
		}
	}
};


