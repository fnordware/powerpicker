//
// Power Picker
//

#ifndef Power_Picker_H
#define Power_Picker_H

#pragma once 


#define PF_DEEP_COLOR_AWARE 1

#include "AEConfig.h"
#include "entry.h"
#include "AE_EffectUI.h"
#include "AE_EffectCBSuites.h"
#include "AE_AdvEffectSuites.h"
#include "String_Utils.h"
#include "Param_Utils.h"
#include "AE_Macros.h"
#include "fnord_SuiteHandler.h"


#ifdef MAC_ENV
#define Macintosh
#endif


#ifdef MSWindows
	#include <windows.h>
#endif

#define USE_HSV				0
#define RGBHSV_SLIDERS		0

#if USE_HSV
#define NAME				"Power Picker HSV"
#else
#define NAME				"Power Picker"
#endif
#define DESCRIPTION			"Color Picker for After Effects"
#define WEBSITE				"www.fnordware.com"
#define RELEASE_DATE		__DATE__
#define	MAJOR_VERSION		1
#define	MINOR_VERSION		2
#define	BUG_VERSION			0
#define	STAGE_VERSION		PF_Stage_RELEASE
#define	BUILD_VERSION		0
#define ID					4111

#ifdef __cplusplus
	extern "C" {
#endif


#ifndef Macintosh
typedef signed short                    SInt16;
typedef long							SInt32;
typedef char *							Ptr;
#endif

enum {
	FILTER_INPUT = 0,
	FILTER_COLOR,
	FILTER_BIG,
	FILTER_LITTLE,
	FILTER_EXTRA_GROUP,
	FILTER_ACTION,
	FILTER_BLEND,
#if USE_HSV || RGBHSV_SLIDERS
	FILTER_RGBHSV_GROUP,
	FILTER_RED,
	FILTER_GREEN,
	FILTER_BLUE,
	FILTER_HUE,
	FILTER_SAT,
	FILTER_VAL,
	FILTER_RGBHSV_END,
#endif
	FILTER_EXTRA_END,
	FILTER_NUM_PARAMS
};

enum {
	COLOR_ID = 1,
	BIG_ID,
	LITTLE_ID,
	EXTRA_ID,
	ACTION_ID,
	BLEND_ID,
	RGBHSV_ID,
	RED_ID,
	GREEN_ID,
	BLUE_ID,
	HUE_ID,
	SAT_ID,
	VAL_ID,
	RGBHSV_END_ID,
	EXTRA_END_ID
};

typedef struct 
{
	int		number;
	char	name[256];
} my_global_data, *my_global_dataPtr, **my_global_dataH;


typedef struct {
	unsigned short	which;
	unsigned short	r;
	unsigned short	g;
	unsigned short	b;
	unsigned short	h;
	unsigned short	s;
	unsigned short	v;
} PickerData;

typedef PickerData *	PickerPtr;
typedef PickerPtr *		PickerHndl;

enum {
	PICKER_RGB = 0,
	PICKER_GBR,
	PICKER_BRG,
	PICKER_HSV,
	PICKER_SVH,
	PICKER_VHS,
	PICKERS_TOTAL
};


enum { // Action options
	ACTION_NONE = 1,
	ACTION_DIVIDER,
	ACTION_FILL,
	ACTION_DIVIDER2,
	ACTION_BLACK_TO_COLOR,
	ACTION_MID_TO_COLOR,
	ACTION_WHITE_TO_COLOR,
	ACTION_TOTAL = ACTION_WHITE_TO_COLOR
};

#define ACTION_MENU_STR		"None|(-|Fill|(-|Map Black to Color|Map Midpoint to Color|Map White to Color"
	


//#define ECW_UI_MAGIC	'WONK'

#define RESID_SERIAL			1001
#define RESID_DATE				1002

#define FREE_DAYS				7
#define BEG_DAYS				FREE_DAYS + 7

#define DEFAULT_RED				0
#define DEFAULT_GREEN			45
#define DEFAULT_BLUE			70
#define DEFAULT_HUE				201
#define DEFAULT_SAT				255
#define DEFAULT_VAL				70

//#define SECTION_KEY				"Power Picker"
//#define DATE_KEY				"Hi There"
//#define SERIAL_KEY				"Cheerios"


#if USE_HSV
#define UPDATE_RGB_SLIDERS( DATA_PTR ) \
			do{ \
				params[FILTER_RED]->u.sd.value = (DATA_PTR)->r; \
				params[FILTER_RED]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE; \
				params[FILTER_GREEN]->u.sd.value = (DATA_PTR)->g; \
				params[FILTER_GREEN]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE; \
				params[FILTER_BLUE]->u.sd.value = (DATA_PTR)->b; \
				params[FILTER_BLUE]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE; \
			} while (0);

#define UPDATE_HSV_SLIDERS( DATA_PTR ) \
			do{ \
				params[FILTER_HUE]->u.sd.value = (DATA_PTR)->h; \
				params[FILTER_HUE]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE; \
				params[FILTER_SAT]->u.sd.value = (DATA_PTR)->s; \
				params[FILTER_SAT]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE; \
				params[FILTER_VAL]->u.sd.value = (DATA_PTR)->v; \
				params[FILTER_VAL]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE; \
			} while (0);


#define UPDATE_SLIDERS( DATA_PTR ) \
			do{ \
				UPDATE_RGB_SLIDERS( (DATA_PTR) ); \
				UPDATE_HSV_SLIDERS( (DATA_PTR) ); \
			} while (0);
#else
#define UPDATE_RGB_SLIDERS
#define UPDATE_HSV_SLIDERS
#define UPDATE_SLIDERS
#endif


#ifndef ResourceID
	#define ResourceID		16000
#endif

#ifndef AboutID
	#define AboutID			ResourceID
	#define RegID			AboutID + 1
	#define BegID			RegID + 1
	
	#define RegStrNormal	15000
	#define RegStrBeg		RegStrNormal + 1
	#define RegStrMandatory	RegStrBeg + 1
#endif


#define UI_BOX_WIDTH			150
#define UI_BOX_HEIGHT			305
#define UI_BOX_LITTLE_HEIGHT	145

#define SQUARE_SIZE				256
#define SQUARE_OFFSET			5
#define RECT_WIDTH				20
#define RECT_APPEARANCE			2  //options are 1: flat color, 2: real color, 3: divided
#define RECT_GAP				RECT_WIDTH + RECT_WIDTH
#define RECT_OFFSET				SQUARE_OFFSET + SQUARE_SIZE + RECT_GAP

#define ARROW_SIZE				4
#define RING_SIZE				3
#define RING_SHADE				200
#define LINE_THICKNESS			1

#define TEXT_GAP				20
#define TEXT_OFFSET				SQUARE_OFFSET + SQUARE_SIZE + TEXT_GAP
#define TEXT_STEP				55
#define TEXT_BOX_HSHIFT			12
#define TEXT_BOX_HEIGHT			14
#define TEXT_BOX_WIDTH			30
#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
#define TEXT_LABEL_HSHIFT		10
#define TEXT_BOX_HILITE_LEXT	7
#else
#define TEXT_LABEL_HSHIFT		3
#define TEXT_BOX_HILITE_LEXT	14
#endif
#define TEXT_LABEL_VSHIFT		3
#define TEXT_VALUE_HSHIFT		3
#define TEXT_VALUE_VSHIFT		3
#define TEXT_BOX_HILITE_REXT	8
#define TEXT_BOX_HILITE_UEXT	6
#define TEXT_BOX_HILITE_DEXT	TEXT_BOX_HILITE_UEXT
#define TEXT_BOX_HILITE_DELTA	60
#define TEXT_BOX_BORDER_REXT	4
#define TEXT_BOX_BORDER_LEXT	(TEXT_BOX_BORDER_REXT - 1)
#define TEXT_BOX_BORDER_UEXT	3
#define TEXT_BOX_BORDER_DEXT	TEXT_BOX_BORDER_UEXT
#define TEXT_BOX_BORDER_DELTA	100
#define TEXT_LITTLE_STEP		23
#define TEXT_LITTLE_GAP			30


enum {
	CLICK_NONE = 0,
	CLICK_SQUARE,
	CLICK_RECT,
	CLICK_SQUARE_H_ARROW,
	CLICK_SQUARE_V_ARROW,
	CLICK_BOXES
};


static PF_Err About (	PF_InData		*in_data,
						PF_OutData		*out_data,
						PF_ParamDef		*params[],
						PF_LayerDef		*output );

static	PF_Err	GlobalSetup(PF_InData		*in_data,
							PF_OutData		*out_data,
							PF_ParamDef		*params[],
							PF_LayerDef		*output );

static	PF_Err	ParamsSetup(PF_InData		*in_data,
							PF_OutData		*out_data,
							PF_ParamDef		*params[],
							PF_LayerDef		*output);

void	RGBtoHSV(	unsigned short red, unsigned short green, unsigned short blue,
					unsigned short *h_out, unsigned short *s_out, unsigned short *v_out);

void 	HSVtoRGB(	unsigned short hue, unsigned short sat, unsigned short val,
						unsigned short *r_out, unsigned short *g_out, unsigned short *b_out );
						
static	PF_Err	SequenceSetup(PF_InData		*in_data,
							PF_OutData		*out_data,
							PF_ParamDef		*params[],
							PF_LayerDef		*output);

static	PF_Err	SequenceSetdown(PF_InData		*in_data,
							PF_OutData		*out_data,
							PF_ParamDef		*params[],
							PF_LayerDef		*output);

static	PF_Err	Render(	PF_InData		*in_data,
						PF_OutData		*out_data,
						PF_ParamDef		*params[],
						PF_LayerDef		*output);

static	PF_Err DoClick(	PF_InData		*in_data,
						PF_OutData		*out_data,
						PF_ParamDef		*params[],
						PF_LayerDef		*output,
						PF_EventExtra	*event_extra);

static	PF_Err DoDrag(	PF_InData		*in_data,
						PF_OutData		*out_data,
						PF_ParamDef		*params[],
						PF_LayerDef		*output,
						PF_EventExtra	*event_extra);
 
PF_Err	HandleEvent(PF_InData		*in_data,
					PF_OutData		*out_data,
					PF_ParamDef		*params[],
					PF_LayerDef		*output,
					PF_EventExtra	*extra);


#if PF_AE_PLUG_IN_VERSION < PF_AE100_PLUG_IN_VERSION
typedef A_long RefconType;
#else
typedef void * RefconType;
#endif


static	PF_Err DrawEvent(	PF_InData		*in_data,
							PF_OutData		*out_data,
							PF_ParamDef		*params[],
							PF_LayerDef		*output,
							PF_EventExtra	*event_extra);

static PF_Err ChangePicker(PF_EventExtra *event_extra);

							
static	PF_Err PixelFunc (	RefconType		refcon,
							A_long			x,
							A_long			y,
							PF_Pixel		*in,
							PF_Pixel		*out);

static	PF_Err PixelFunc16 (RefconType		refcon,
							A_long			x,
							A_long			y,
							PF_Pixel16		*in,
							PF_Pixel16		*out);


static	PF_Err ChangeCursor(PF_InData		*in_data,
							PF_OutData		*out_data,
							PF_ParamDef		*params[],
							PF_LayerDef		*output,
							PF_EventExtra	*event_extra);
#ifdef __cplusplus
	}
#endif


#endif  // Power_Picker_H
