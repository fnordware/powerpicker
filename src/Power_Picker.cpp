//
// Power Picker
//
											

#include "Power_Picker.h"


DllExport PF_Err 
main (	
	PF_Cmd			cmd,
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	void			*extra )
{
		
	PF_Err		err = PF_Err_NONE;

	try {
		switch (cmd) {
		
		case PF_Cmd_ABOUT:
			err = About(in_data, out_data, params, output);
			break;
		case PF_Cmd_GLOBAL_SETUP:
			err = GlobalSetup(in_data, out_data, params, output);
			break;
		case PF_Cmd_PARAMS_SETUP:
			err = ParamsSetup(in_data, out_data, params, output);
			break;
		case PF_Cmd_SEQUENCE_SETUP:
		case PF_Cmd_SEQUENCE_RESETUP:
			err = SequenceSetup(in_data,out_data,params,output);
			break;
		case PF_Cmd_SEQUENCE_SETDOWN:
			err = SequenceSetdown(in_data,out_data,params,output);
			break;
		case PF_Cmd_RENDER:
			err = Render(in_data, out_data, params, output);
			break;
		case PF_Cmd_EVENT:
			err = HandleEvent(in_data, out_data, params, output, (PF_EventExtra	*)extra);
			break;
		default:
			break;
		}
	}
	catch(PF_Err &thrown_err) { err = thrown_err; }
	catch(...) { err = PF_Err_INTERNAL_STRUCT_DAMAGED; }
	
	return err;
}

static PF_Err 
About (	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output )
{
	PF_Err err = PF_Err_NONE;

	PF_SPRINTF(	out_data->return_msg, 
				"%s\r   by Brendan Bolles\r\r(c) 2002-2010 fnord\r   v%d.%d - %s\r\r%s",
				NAME, 
				MAJOR_VERSION, 
				MINOR_VERSION,
				RELEASE_DATE,
				WEBSITE);


	return err;
}


static int DaysSince2000(int year, int month, int day)
{
	int total = 0;
	
	total += 365 * (year - 2000);
	
	total += (year - 2000) / 4; // leap days
	
	
	int month_days[] = { 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31 };
	
	if(year % 4 == 0)
		month_days[1] = 29;

	for(int m = 1; m < month; m++)
		total += month_days[m - 1];
		
	
	total += day - 1;
	
	return total;
}

static int DaysPast(int cur_year, int cur_month, int cur_day,
					int past_year, int past_month, int past_day)
{
	return (DaysSince2000(cur_year, cur_month, cur_day) -
				DaysSince2000(past_year, past_month, past_day));
}


static PF_Err 
GlobalSetup (
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output )
{
	PF_Err					err			= PF_Err_NONE;
							
	out_data->out_flags		=	PF_OutFlag_CUSTOM_UI | 
								PF_OutFlag_PIX_INDEPENDENT | 
								PF_OutFlag_USE_OUTPUT_EXTENT |
								PF_OutFlag_KEEP_RESOURCE_OPEN |
								PF_OutFlag_DEEP_COLOR_AWARE;
								
	out_data->my_version = PF_VERSION(	MAJOR_VERSION, 
										MINOR_VERSION,	
										BUG_VERSION, 
										STAGE_VERSION, 
										BUILD_VERSION);

	return err;
}

static PF_Err
ParamsSetup(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output)
{
	PF_Err 			err = PF_Err_NONE;
	PF_ParamDef		def;

	AEFX_CLR_STRUCT(def);
#if USE_HSV
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
#else
	def.flags		=	PF_ParamFlag_SUPERVISE;
#endif
	PF_ADD_COLOR(	"Color",  
					DEFAULT_RED, 
					DEFAULT_GREEN, 
					DEFAULT_BLUE,
					COLOR_ID);
	
					
	AEFX_CLR_STRUCT(def);
	def.ui_flags =	PF_PUI_CONTROL;
	def.flags =		PF_ParamFlag_COLLAPSE_TWIRLY;
	def.ui_width	=	UI_BOX_WIDTH;
	def.ui_height 	= UI_BOX_HEIGHT;
	PF_ADD_NULL(	"Big", BIG_ID);


	AEFX_CLR_STRUCT(def);
	def.ui_flags =	PF_PUI_CONTROL;
	//def.flags =		PF_ParamFlag_COLLAPSE_TWIRLY;
	def.ui_width	=	UI_BOX_WIDTH;
	def.ui_height 	= UI_BOX_LITTLE_HEIGHT;
	PF_ADD_NULL(	"Little", LITTLE_ID);


	AEFX_CLR_STRUCT(def);
	PF_ADD_TOPIC("More", EXTRA_ID);
	
	
	AEFX_CLR_STRUCT(def);
	//def.flags = 	PF_ParamFlag_CANNOT_TIME_VARY;
	PF_ADD_POPUP(	"Action",
					ACTION_TOTAL, //number of choices
					ACTION_FILL, //default
					ACTION_MENU_STR,
					ACTION_ID);
	
	
	AEFX_CLR_STRUCT(def);
	PF_ADD_PERCENT("Blend with Original", 0, ID);
	
	
#if USE_HSV || RGBHSV_SLIDERS// only include this stuff for HSV

	AEFX_CLR_STRUCT(def);
	PF_ADD_TOPIC("RGB HSV Values", RGBHSV_ID);

	AEFX_CLR_STRUCT(def);
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
	PF_ADD_SLIDER(	"Red",
					0, 255, 0, 255,
					DEFAULT_RED,
					RED_ID);


	AEFX_CLR_STRUCT(def);
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
	PF_ADD_SLIDER(	"Green",
					0, 255, 0, 255,
					DEFAULT_GREEN,
					GREEN_ID);


	AEFX_CLR_STRUCT(def);
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
	PF_ADD_SLIDER(	"Blue",
					0, 255, 0, 255,
					DEFAULT_BLUE,
					BLUE_ID);
					

	AEFX_CLR_STRUCT(def);
#if USE_HSV
	def.flags		=	PF_ParamFlag_SUPERVISE;
#else
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
#endif
	PF_ADD_SLIDER(	"Hue",
					0, 360, 0, 360,
					DEFAULT_HUE,
					HUE_ID);


	AEFX_CLR_STRUCT(def);
#if USE_HSV
	def.flags		=	PF_ParamFlag_SUPERVISE;
#else
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
#endif
	PF_ADD_SLIDER(	"Saturation",
					0, 255, 0, 255,
					DEFAULT_SAT,
					SAT_ID);

					
	AEFX_CLR_STRUCT(def);
#if USE_HSV
	def.flags		=	PF_ParamFlag_SUPERVISE;
#else
	def.flags		=	PF_ParamFlag_SUPERVISE |
						PF_ParamFlag_CANNOT_TIME_VARY;
#endif
	PF_ADD_SLIDER(	"Value",
					0, 255, 0, 255,
					DEFAULT_VAL,
					VAL_ID);


	AEFX_CLR_STRUCT(def);
	PF_END_TOPIC(RGBHSV_END_ID);

#endif // USE_HSV

	AEFX_CLR_STRUCT(def);
	PF_END_TOPIC(EXTRA_END_ID);


	if (!err) 
	{
		PF_CustomUIInfo			ci;

		AEFX_CLR_STRUCT(ci);
		
		ci.events				= PF_CustomEFlag_EFFECT;
 		
		ci.comp_ui_width		= ci.comp_ui_height = 0;
		ci.comp_ui_alignment	= PF_UIAlignment_NONE;
		
		ci.layer_ui_width		= 0;
		ci.layer_ui_height		= 0;
		ci.layer_ui_alignment	= PF_UIAlignment_NONE;
		
		ci.preview_ui_width		= 0;
		ci.preview_ui_height	= 0;
		ci.layer_ui_alignment	= PF_UIAlignment_NONE;

		err = (*(in_data->inter.register_ui))(in_data->effect_ref, &ci);
	}
	
	out_data->num_params = FILTER_NUM_PARAMS;

	return err;
}

static PF_Err
ParamsSupervise (
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_UserChangedParamExtra *extra)
{
	PF_Err err = PF_Err_NONE;
	
	PickerPtr pData = (PickerPtr)PF_LOCK_HANDLE(in_data->sequence_data);
	
	
	switch(extra->param_index)
	{
		case FILTER_COLOR:
			if(	pData->r != params[FILTER_COLOR]->u.cd.value.red ||
				pData->g != params[FILTER_COLOR]->u.cd.value.green ||
				pData->b != params[FILTER_COLOR]->u.cd.value.blue  )
			{
				pData->r = params[FILTER_COLOR]->u.cd.value.red;
				pData->g = params[FILTER_COLOR]->u.cd.value.green;
				pData->b = params[FILTER_COLOR]->u.cd.value.blue;
				
				RGBtoHSV(pData->r, pData->g, pData->b, &pData->h, &pData->s, &pData->v);
				
				UPDATE_SLIDERS( pData );
			}
			break;

#if USE_HSV	|| RGBHSV_SLIDERS
		case FILTER_RED:
			if( pData->r != params[FILTER_RED]->u.sd.value )
			{
				pData->r = params[FILTER_COLOR]->u.cd.value.red = params[FILTER_RED]->u.sd.value;
				params[FILTER_COLOR]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
				
				RGBtoHSV(pData->r, pData->g, pData->b, &pData->h, &pData->s, &pData->v);
				
				UPDATE_HSV_SLIDERS( pData );
			}
			break;
			
		case FILTER_GREEN:
			if( pData->g != params[FILTER_GREEN]->u.sd.value )
			{
				pData->g = params[FILTER_COLOR]->u.cd.value.green = params[FILTER_GREEN]->u.sd.value;
				params[FILTER_COLOR]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
				
				RGBtoHSV(pData->r, pData->g, pData->b, &pData->h, &pData->s, &pData->v);
				
				UPDATE_HSV_SLIDERS( pData );
			}
			break;
			
		case FILTER_BLUE:
			if( pData->b != params[FILTER_BLUE]->u.sd.value )
			{
				pData->b = params[FILTER_COLOR]->u.cd.value.blue = params[FILTER_BLUE]->u.sd.value;
				params[FILTER_COLOR]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
				
				RGBtoHSV(pData->r, pData->g, pData->b, &pData->h, &pData->s, &pData->v);
				
				UPDATE_HSV_SLIDERS( pData );
			}
			break;
			
		case FILTER_HUE:
			if( pData->h != params[FILTER_HUE]->u.sd.value )
			{
				pData->h = params[FILTER_HUE]->u.sd.value;
				
				HSVtoRGB(pData->h, pData->s, pData->v, &pData->r, &pData->g, &pData->b);

				params[FILTER_COLOR]->u.cd.value.red = pData->r;
				params[FILTER_COLOR]->u.cd.value.green = pData->g;
				params[FILTER_COLOR]->u.cd.value.blue = pData->b;
				params[FILTER_COLOR]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
				
				UPDATE_RGB_SLIDERS( pData );
			}
			break;

		case FILTER_SAT:
			if( pData->s != params[FILTER_SAT]->u.sd.value )
			{
				pData->s = params[FILTER_SAT]->u.sd.value;
				
				HSVtoRGB(pData->h, pData->s, pData->v, &pData->r, &pData->g, &pData->b);

				params[FILTER_COLOR]->u.cd.value.red = pData->r;
				params[FILTER_COLOR]->u.cd.value.green = pData->g;
				params[FILTER_COLOR]->u.cd.value.blue = pData->b;
				params[FILTER_COLOR]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
				
				UPDATE_RGB_SLIDERS( pData );
			}
			break;

		case FILTER_VAL:
			if( pData->v != params[FILTER_VAL]->u.sd.value )
			{
				pData->v = params[FILTER_VAL]->u.sd.value;
				
				HSVtoRGB(pData->h, pData->s, pData->v, &pData->r, &pData->g, &pData->b);

				params[FILTER_COLOR]->u.cd.value.red = pData->r;
				params[FILTER_COLOR]->u.cd.value.green = pData->g;
				params[FILTER_COLOR]->u.cd.value.blue = pData->b;
				params[FILTER_COLOR]->uu.change_flags = PF_ChangeFlag_CHANGED_VALUE;
				
				UPDATE_RGB_SLIDERS( pData );
			}
			break;
#endif //USE_HSV			
	}
	
	
	
	
/*	else if(extra->param_index == FILTER_BIG)
	{
		if( !(params[FILTER_BIG]->flags & PF_ParamFlag_COLLAPSE_TWIRLY) )
		{
			params[FILTER_LITTLE]->flags |= PF_ParamFlag_COLLAPSE_TWIRLY;
			params[FILTER_LITTLE]->uu.change_flags	= PF_ChangeFlag_CHANGED_VALUE;
		}
	}
	else if(extra->param_index == FILTER_LITTLE)
	{
		if( !(params[FILTER_LITTLE]->flags & PF_ParamFlag_COLLAPSE_TWIRLY) )
		{
			params[FILTER_BIG]->flags |= PF_ParamFlag_COLLAPSE_TWIRLY;
			params[FILTER_BIG]->uu.change_flags	= PF_ChangeFlag_CHANGED_VALUE;
		}
	}
*/	
	PF_UNLOCK_HANDLE(in_data->sequence_data);

	return err;
}


static	PF_Err
SequenceSetup(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output)
{
	PF_Err			err = PF_Err_NONE;
	
	PickerPtr pData;
	
	if((in_data->sequence_data == NULL) || (PF_GET_HANDLE_SIZE(in_data->sequence_data) < sizeof(PickerData)) )
	{
		if(in_data->sequence_data)
		{
			PF_RESIZE_HANDLE(sizeof(PickerData), in_data->sequence_data);
			pData = (PickerPtr)PF_LOCK_HANDLE(in_data->sequence_data);
		}
		else
		{
			out_data->sequence_data = PF_NEW_HANDLE(sizeof(PickerData));
			pData = (PickerPtr)PF_LOCK_HANDLE(out_data->sequence_data);
		}
		
		pData->which = PICKER_HSV;
		pData->r	= DEFAULT_RED;
		pData->g	= DEFAULT_GREEN;
		pData->b	= DEFAULT_BLUE;
		
		RGBtoHSV(pData->r, pData->g, pData->b, &pData->h, &pData->s, &pData->v);

		PF_UNLOCK_HANDLE(out_data->sequence_data);
	}
	
	return err;
}


static	PF_Err
SequenceSetdown(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output)
{
	PF_Err			err = PF_Err_NONE;
	
	if(in_data->sequence_data)
	{
		PF_DISPOSE_HANDLE(in_data->sequence_data);
		in_data->sequence_data = NULL;
	}
			
	return err;
}


static PF_Err	
Render(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output)
{
	PF_Err				err		= PF_Err_NONE,
						err2	= PF_Err_NONE;
						
	AEGP_SuiteHandler	suites(in_data->pica_basicP);

	PF_Iterate8Suite	*i8sP	= suites.PFIterate8Suite();
	PF_Iterate16Suite	*i16sP	= suites.PFIterate16Suite();
	
	PF_Boolean deepB = PF_WORLD_IS_DEEP(output);

	if(!err)
	{
		if(deepB)
		{
			err = i16sP->iterate(in_data,
								0,
								(output->extent_hint.bottom - output->extent_hint.top),
								&params[FILTER_INPUT]->u.ld,
								&output->extent_hint,
								//(long)&params[FILTER_COLOR]->u.cd.value,
								(RefconType)params,
								PixelFunc16,
								output);
		}
		else
		{
			err = i8sP->iterate(in_data,
								0,
								(output->extent_hint.bottom - output->extent_hint.top),
								&params[FILTER_INPUT]->u.ld,
								&output->extent_hint,
								//(long)&params[FILTER_COLOR]->u.cd.value,
								(RefconType)params,
								PixelFunc,
								output);
		}
	}
	
	
	if (err2 && !err)
	{
		err = err2;
	}
	
	return err;
}

#ifndef MAX
#define MAX(A,B)			( (A) > (B) ? (A) : (B))
#endif
	
static PF_Err 
PixelFunc (	
	RefconType refcon,
	A_long x,
	A_long y,
	PF_Pixel *in,
	PF_Pixel *out)
{
	PF_ParamDef **params = (PF_ParamDef **)refcon;

#if USE_HSV
#define H_P params[FILTER_HUE]->u.sd.value
#define S_P params[FILTER_SAT]->u.sd.value
#define V_P params[FILTER_VAL]->u.sd.value

#define SET_RGB_P() \
			unsigned short r_s, g_s, b_s; \
			HSVtoRGB( H_P, S_P, V_P, &r_s, &g_s, &b_s); \
			r_p = r_s; g_p = g_s; b_p = b_s;
			
//#define R_P r_s
//#define G_P g_s
//#define B_P b_s

#else

#define R_P params[FILTER_COLOR]->u.cd.value.red
#define G_P params[FILTER_COLOR]->u.cd.value.green
#define B_P params[FILTER_COLOR]->u.cd.value.blue

#define SET_RGB_P()	r_p = R_P; g_p = G_P; b_p = B_P;

#endif

//Windows SDK still at r2 - watch out for the max value of FILTER_BLEND
#define BLEND 				(params[FILTER_BLEND]->u.fd.value / 6553500.0)
#define BLEND_FUNC(IN,OUT)	( ( BLEND * (IN) ) + ( ( 1.0 - BLEND ) * (OUT) ) )
#define BLEND_ORIG(IN,OUT)	( BLEND == 0.0 )?( (OUT) ) : ( BLEND == 1.0 )?( (IN) ) : BLEND_FUNC( (IN), (OUT) )
//#define BLEND_ORIG(IN,OUT)	( ( 0.5 * (IN) ) + ( ( 1.0 - 0.5 ) * (OUT) ) )

#define MAX_VAL 255.0
#define MIN_VAL 0.0
#define HALF_VAL 127.5
#define LUM (lum_i / MAX_VAL)

	switch(params[FILTER_ACTION]->u.pd.value)
	{
		case ACTION_NONE:
			out->red 	= in->red;
			out->green 	= in->green;
			out->blue 	= in->blue;
			break;
		
		case ACTION_FILL:
			do {
				float r_p, g_p, b_p;

				SET_RGB_P();
				out->red 	= BLEND_ORIG(in->red, r_p);
				out->green 	= BLEND_ORIG(in->green, g_p);
				out->blue 	= BLEND_ORIG(in->blue, b_p);
			} while (0);
			break;
		
		case ACTION_BLACK_TO_COLOR:
			do {
				float 	r_i, g_i, b_i, lum_i,
						r_p, g_p, b_p,
						r_o, g_o, b_o;

				SET_RGB_P();
				r_i = in->red; g_i = in->green; b_i = in->blue;
				
				lum_i = (0.299 * r_i) + (0.587 * g_i) + (0.114 * b_i);

				r_o	= r_p + ( LUM * (MAX_VAL - r_p) );
				g_o	= g_p + ( LUM * (MAX_VAL - g_p) );
				b_o	= b_p + ( LUM * (MAX_VAL - b_p) );
				
				out->red   = BLEND_ORIG(r_i, r_o);
				out->green = BLEND_ORIG(g_i, g_o);
				out->blue  = BLEND_ORIG(b_i, b_o);

			} while (0);
			break;

		case ACTION_MID_TO_COLOR:
			do {
				float 	r_i, g_i, b_i, lum_i,
						r_p, g_p, b_p,
						r_o, g_o, b_o;
			
				SET_RGB_P();
				r_i = in->red; g_i = in->green; b_i = in->blue;
				
				lum_i = (0.299 * r_i) + (0.587 * g_i) + (0.114 * b_i);
				
				if(lum_i >= HALF_VAL)
				{
					r_o = MIN ( r_p + ( ( (lum_i - HALF_VAL) / HALF_VAL ) * (MAX_VAL - r_p) ), MAX_VAL);
					g_o = MIN ( g_p + ( ( (lum_i - HALF_VAL) / HALF_VAL ) * (MAX_VAL - g_p) ), MAX_VAL);
					b_o = MIN ( b_p + ( ( (lum_i - HALF_VAL) / HALF_VAL ) * (MAX_VAL - b_p) ), MAX_VAL);
				}
				else
				{
					r_o = MAX ( r_p - ( ( (HALF_VAL - lum_i) / HALF_VAL ) * (r_p) ), MIN_VAL);
					g_o = MAX ( g_p - ( ( (HALF_VAL - lum_i) / HALF_VAL ) * (g_p) ), MIN_VAL);
					b_o = MAX ( b_p - ( ( (HALF_VAL - lum_i) / HALF_VAL ) * (b_p) ), MIN_VAL);
				}
				
				out->red   = BLEND_ORIG(r_i, r_o);
				out->green = BLEND_ORIG(g_i, g_o);
				out->blue  = BLEND_ORIG(b_i, b_o);
				
			} while (0);
			break;

		case ACTION_WHITE_TO_COLOR:
			do {
				float 	r_i, g_i, b_i, lum_i,
						r_p, g_p, b_p,
						r_o, g_o, b_o;

				SET_RGB_P();
				r_i = in->red; g_i = in->green; b_i = in->blue;
				
				lum_i = (0.299 * r_i) + (0.587 * g_i) + (0.114 * b_i);
				
				r_o	= LUM * r_p;
				g_o	= LUM * g_p;
				b_o	= LUM * b_p;

				out->red   = BLEND_ORIG(r_i, r_o);
				out->green = BLEND_ORIG(g_i, g_o);
				out->blue  = BLEND_ORIG(b_i, b_o);
			break;
			} while (0);
			break;
/*
		case ACTION_HUE:
			do {
				unsigned short r, g, b, h, s, v,
				r_p, g_p, b_p, h_p, s_p, v_p,
				r_o, g_o, b_o, h_o, s_o, v_o;
			
				r = in->red; g = in->green; b = in->blue;
				r_p = R_P;
				g_p = G_P;
				b_p = B_P;
				
				RGBtoHSV(r, g, b, &h, &s, &v);
				RGBtoHSV(r_p, g_p, b_p, &h_p, &s_p, &v_p);
				
				h_o = h_p; s_o = s; v_o = v;
				
				HSVtoRGB(h_o, s_o, v_o, &r_o, &g_o, &b_o);
				
				out->red 	= r_o;
				out->green 	= g_o;
				out->blue 	= b_o;
			} while (0);
			break;
*/			
		default: //same as FILL
			do {
				float r_p, g_p, b_p;

				SET_RGB_P();
				out->red 	= BLEND_ORIG(in->red, r_p);
				out->green 	= BLEND_ORIG(in->green, g_p);
				out->blue 	= BLEND_ORIG(in->blue, b_p);
			} while (0);
			break;
	}

	out->alpha  = in->alpha;


	return PF_Err_NONE;
}

static PF_Err 
PixelFunc16 (	
	RefconType refcon,
	A_long x,
	A_long y,
	PF_Pixel16 *in,
	PF_Pixel16 *out)
{
	PF_ParamDef **params = (PF_ParamDef **)refcon;

#define CONV_8_16(NUM)	( (NUM) >= 127 ) ? ( ( (NUM) << 7 ) + 1 ) : ( (NUM) << 7 )

#if USE_HSV
#define H_P params[FILTER_HUE]->u.sd.value
#define S_P params[FILTER_SAT]->u.sd.value
#define V_P params[FILTER_VAL]->u.sd.value

#define SET_RGB_P_16() \
			unsigned short r_s, g_s, b_s; \
			HSVtoRGB( H_P, S_P, V_P, &r_s, &g_s, &b_s); \
			r_p = CONV_8_16(r_s); g_p = CONV_8_16(g_s); b_p = CONV_8_16(b_s);
			
//#define R_P_16 r_s
//#define G_P_16 g_s
//#define B_P_16 b_s

#else

#define R_P_16 CONV_8_16(params[FILTER_COLOR]->u.cd.value.red)
#define G_P_16 CONV_8_16(params[FILTER_COLOR]->u.cd.value.green)
#define B_P_16 CONV_8_16(params[FILTER_COLOR]->u.cd.value.blue)

#define SET_RGB_P_16()	r_p = R_P_16; g_p = G_P_16; b_p = B_P_16;

#endif


#define MAX_VAL_16 0x8000
#define MIN_VAL_16 0.0
#define HALF_VAL_16 ( MAX_VAL_16 / 2 )
#define LUM_16 (lum_i / MAX_VAL_16)


	switch(params[FILTER_ACTION]->u.pd.value)
	{
		case ACTION_NONE:
			out->red 	= in->red;
			out->green 	= in->green;
			out->blue 	= in->blue;
			break;
		
		case ACTION_FILL:
			do {
				float r_p, g_p, b_p;
				
				SET_RGB_P_16()
				out->red 	= BLEND_ORIG(in->red, r_p);
				out->green 	= BLEND_ORIG(in->green, g_p);
				out->blue 	= BLEND_ORIG(in->blue, b_p);
			} while (0);
			break;
		
		case ACTION_BLACK_TO_COLOR:
			do {
				float 	r_i, g_i, b_i, lum_i,
						r_p, g_p, b_p,
						r_o, g_o, b_o;

				SET_RGB_P_16()
				r_i = in->red; g_i = in->green; b_i = in->blue;
				
				lum_i = (0.299 * r_i) + (0.587 * g_i) + (0.114 * b_i);

				r_o	= r_p + ( LUM_16 * (MAX_VAL_16 - r_p) );
				g_o	= g_p + ( LUM_16 * (MAX_VAL_16 - g_p) );
				b_o	= b_p + ( LUM_16 * (MAX_VAL_16 - b_p) );
				
				out->red = 	 BLEND_ORIG(r_i, r_o);
				out->green = BLEND_ORIG(g_i, g_o);
				out->blue =  BLEND_ORIG(b_i, b_o);

			} while (0);
			break;

		case ACTION_MID_TO_COLOR:
			do {
				float 	r_i, g_i, b_i, lum_i,
						r_p, g_p, b_p,
						r_o, g_o, b_o;
			
				SET_RGB_P_16()
				r_i = in->red; g_i = in->green; b_i = in->blue;
				
				lum_i = (0.299 * r_i) + (0.587 * g_i) + (0.114 * b_i);
				
				if(lum_i >= HALF_VAL_16)
				{
					r_o = MIN ( r_p + ( ( (lum_i - HALF_VAL_16) / HALF_VAL_16 ) * (MAX_VAL_16 - r_p) ), MAX_VAL_16);
					g_o = MIN ( g_p + ( ( (lum_i - HALF_VAL_16) / HALF_VAL_16 ) * (MAX_VAL_16 - g_p) ), MAX_VAL_16);
					b_o = MIN ( b_p + ( ( (lum_i - HALF_VAL_16) / HALF_VAL_16 ) * (MAX_VAL_16 - b_p) ), MAX_VAL_16);
				}
				else
				{
					r_o = MAX ( r_p - ( ( (HALF_VAL_16 - lum_i) / HALF_VAL_16 ) * (r_p) ), MIN_VAL_16);
					g_o = MAX ( g_p - ( ( (HALF_VAL_16 - lum_i) / HALF_VAL_16 ) * (g_p) ), MIN_VAL_16);
					b_o = MAX ( b_p - ( ( (HALF_VAL_16 - lum_i) / HALF_VAL_16 ) * (b_p) ), MIN_VAL_16);
				}
				
				out->red = 	 BLEND_ORIG(r_i, r_o);
				out->green = BLEND_ORIG(g_i, g_o);
				out->blue =  BLEND_ORIG(b_i, b_o);
				
			} while (0);
			break;

		case ACTION_WHITE_TO_COLOR:
			do {
				float 	r_i, g_i, b_i, lum_i,
						r_p, g_p, b_p,
						r_o, g_o, b_o;

				SET_RGB_P_16()
				r_i = in->red; g_i = in->green; b_i = in->blue;
				
				lum_i = (0.299 * r_i) + (0.587 * g_i) + (0.114 * b_i);
				
				r_o	= LUM_16 * r_p;
				g_o	= LUM_16 * g_p;
				b_o	= LUM_16 * b_p;

				out->red = 	 BLEND_ORIG(r_i, r_o);
				out->green = BLEND_ORIG(g_i, g_o);
				out->blue =  BLEND_ORIG(b_i, b_o);
			break;
			} while (0);
			break;

		default: //same as FILL
			do {
				float r_p, g_p, b_p;
				
				SET_RGB_P_16()
				out->red 	= BLEND_ORIG(in->red, r_p);
				out->green 	= BLEND_ORIG(in->green, g_p);
				out->blue 	= BLEND_ORIG(in->blue, b_p);
			} while (0);
			break;
	}

	out->alpha  = in->alpha;


	return PF_Err_NONE;
}
