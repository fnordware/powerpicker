//
// Power Picker
//

#include "Power_Picker.h"

#include <stdio.h>
#include <string.h>

#if PF_AE_PLUG_IN_VERSION >= PF_AE100_PLUG_IN_VERSION
#define USE_DRAWBOT	1
#endif

#if USE_DRAWBOT
	#define WIN_HDC			suites, supplier_ref, surface_ref, 
	#define WIN_HDC_TYPE	AEGP_SuiteHandler &suites, DRAWBOT_SupplierRef supplier_ref, DRAWBOT_SurfaceRef surface_ref, 
	#define WIN_OFFSET	0
#else
	#ifdef Macintosh
	#define WIN_HDC
	#define WIN_HDC_TYPE
	#define WIN_OFFSET	0
	#else
	#define WIN_HDC			ecw_hdc, 
	#define WIN_HDC_TYPE	HDC	ecw_hdc, 
	#define WIN_OFFSET	0
	#endif
#endif

#define CLICK_HIT		event_extra->u.do_click.continue_refcon[0]
#define IN_DATA			(*((PF_InData **)&event_extra->u.do_click.continue_refcon[2]))
#define PICKER_DATA		((PickerPtr)*((IN_DATA)->sequence_data))
#define LITTLE_UI		(event_extra->effect_win.index == FILTER_LITTLE)

	#define PP_NEW_HANDLE(SIZE) \
		(*IN_DATA->utils->host_new_handle)((SIZE))

	#define PP_DISPOSE_HANDLE(PF_HANDLE) \
		(*IN_DATA->utils->host_dispose_handle)((PF_Handle)(PF_HANDLE))

	#define PP_LOCK_HANDLE(PF_HANDLE) \
		(*IN_DATA->utils->host_lock_handle)((PF_Handle)(PF_HANDLE))

	#define PP_UNLOCK_HANDLE(PF_HANDLE) \
		(*IN_DATA->utils->host_unlock_handle)((PF_Handle)(PF_HANDLE))

	#define PP_GET_HANDLE_SIZE(PF_HANDLE) \
		(*IN_DATA->utils->host_get_handle_size)((PF_Handle)(PF_HANDLE))

#define V_OFFSET		event_extra->effect_win.current_frame.top
#define H_OFFSET		event_extra->effect_win.current_frame.left

#define LITTLE_HALF(NUM)	( (LITTLE_UI)? ( (NUM) / 2 ) : (NUM) )
#define LITTLE_DOUBLE(NUM)	( (LITTLE_UI)? ( (NUM) * 2 ) + ( (NUM) > 64?1:0) : (NUM) )

#define INIT_RECTF32(RECT)	{0.5f + RECT.left, 0.5f + RECT.top, RECT.right - RECT.left, RECT.bottom - RECT.top}

PF_Err 
HandleEvent(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*extra)
{
	PF_Err		err		= PF_Err_NONE;
	
	if (!err) 
	{
		PF_LOCK_HANDLE(in_data->sequence_data);

		// storing a pointer to in-data in the refcon, don't ask me why
		PF_InData **in_data_holder = (PF_InData **)&extra->u.do_click.continue_refcon[2];
		*in_data_holder = in_data;
	
		switch (extra->e_type) 
		{
			case PF_Event_DO_CLICK:
				err =	DoClick(in_data, out_data, params, output, extra);
				break;
			
			case PF_Event_DRAG:
				err =	DoDrag(in_data, out_data, params, output, extra);
				break;
			
			case PF_Event_DRAW:
				err =	DrawEvent(in_data, out_data, params, output, extra);
				break;

			case PF_Event_ADJUST_CURSOR:
				err	=	ChangeCursor(in_data, out_data, params, output, extra);
				break;
		}
		
		PF_UNLOCK_HANDLE(in_data->sequence_data);
	}
	
	return err;
}


static int ClickedWhat(PF_EventExtra *event_extra)
{
	PF_Point mouse_down = event_extra->u.do_click.screen_point;
	PF_Rect ui_area = event_extra->effect_win.current_frame;

#if WIN_OFFSET
	mouse_down.h++; mouse_down.v++; // for the annoying windows offset issue
#endif
	
	if(	mouse_down.h >= (ui_area.left + SQUARE_OFFSET) &&
		mouse_down.h <= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) &&
		mouse_down.v >= (ui_area.top  + SQUARE_OFFSET) &&
		mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) )
	{
		return (CLICK_HIT = CLICK_SQUARE);
	}
	else if(mouse_down.h >= (ui_area.left + LITTLE_HALF(RECT_OFFSET)) &&
			mouse_down.h <= (ui_area.left + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + 2) &&
			mouse_down.v >= (ui_area.top  + SQUARE_OFFSET) &&
			mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) )
	{
		return (CLICK_HIT = CLICK_RECT);
	}
	else if(mouse_down.h >= (ui_area.left + SQUARE_OFFSET) &&
			mouse_down.h <= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) &&
			mouse_down.v >= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) &&
			mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) + ARROW_SIZE + 2) )
	{
		return (CLICK_HIT = CLICK_SQUARE_H_ARROW);
	}
	else if(mouse_down.h >= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) &&
			mouse_down.h <= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) + ARROW_SIZE + 2) &&
			mouse_down.v >= (ui_area.top  + SQUARE_OFFSET) &&
			mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) )
	{
		return (CLICK_HIT = CLICK_SQUARE_V_ARROW);
	}
	else if(LITTLE_UI &&
			mouse_down.h >= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP - (TEXT_LABEL_HSHIFT + TEXT_BOX_HILITE_LEXT)) &&
			mouse_down.h <= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP + TEXT_BOX_WIDTH + TEXT_BOX_HILITE_REXT) &&
			mouse_down.v >= (ui_area.top  + SQUARE_OFFSET - TEXT_BOX_HILITE_UEXT) &&
			mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + TEXT_BOX_HEIGHT + TEXT_BOX_HILITE_DEXT + ((PICKERS_TOTAL-1) * TEXT_LITTLE_STEP) ) )
	{
		return (CLICK_HIT = CLICK_BOXES);
	}
	else if(mouse_down.h >= (ui_area.left + SQUARE_OFFSET + TEXT_BOX_HSHIFT - (TEXT_LABEL_HSHIFT + TEXT_BOX_HILITE_LEXT)) &&
			mouse_down.h <= (ui_area.left + SQUARE_OFFSET + TEXT_BOX_WIDTH + TEXT_BOX_HSHIFT + ((PICKERS_TOTAL-1) * TEXT_STEP) + TEXT_BOX_HILITE_REXT) &&
			mouse_down.v >= (ui_area.top  + TEXT_OFFSET - TEXT_BOX_HILITE_UEXT) &&
			mouse_down.v <= (ui_area.top  + TEXT_OFFSET + TEXT_BOX_HEIGHT + TEXT_BOX_HILITE_DEXT) )
	{
		return (CLICK_HIT = CLICK_BOXES);
	}
	else
		return (CLICK_HIT = CLICK_NONE);
}


static PF_Err 
DoClick(
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err	err = PF_Err_NONE;
	//PF_ContextH		contextH	= event_extra->contextH;

	int hit;

#if USE_DRAWBOT
#else // USE_DRAWBOT
#if MSWindows
	void*			dp = (*(event_extra->contextH))->cgrafptr;
	HDC				ecw_hdc	=	0;
	PF_GET_CGRAF_DATA(dp, PF_CGrafData_HDC, &ecw_hdc);
#endif
#endif // USE_DRAWBOT
	
	hit = ClickedWhat(event_extra);
	
	if( hit == CLICK_BOXES )
	{
		ChangePicker(event_extra);

		event_extra->u.do_click.send_drag = FALSE;

		event_extra->evt_out_flags |= PF_EO_HANDLED_EVENT;
	}
	else if(hit)
	{
		//err = DoDrag(in_data, out_data, params, output, event_extra);
	
		event_extra->u.do_click.send_drag = TRUE;

		event_extra->evt_out_flags |= PF_EO_HANDLED_EVENT;
	}

	return	err;
}


#define MAX(A,B)			( (A) > (B) ? (A) : (B))
#define MAX3(A,B,C)			MAX( (A) , MAX( (B), (C) ) )
#define MIN3(A,B,C)			MIN( (A) , MIN( (B), (C) ) )

void RGBtoHSV(	unsigned short red, unsigned short green, unsigned short blue,
						unsigned short *h_out, unsigned short *s_out, unsigned short *v_out)
{
	//going from 0-255 here

	float 	r = red,
			g = green,
			b = blue,
			r_p, g_p, b_p, h, s, v;
	
	
	s = ( MAX3(r,g,b) - MIN3(r,g,b) ) / MAX3(r,g,b);
	
	v = MAX3(r,g,b);
	
	r_p = ( MAX3(r,g,b) - r ) / ( MAX3(r,g,b) - MIN3(r,g,b) );
	g_p = ( MAX3(r,g,b) - g ) / ( MAX3(r,g,b) - MIN3(r,g,b) );
	b_p = ( MAX3(r,g,b) - b ) / ( MAX3(r,g,b) - MIN3(r,g,b) );

	
	if(s == 0)
		h = 0;
	else if(MAX3(r,g,b) == r)
	{
		if(MIN3(r,g,b) == g )
			h = (5 + b_p) * 60;
		else
			h = (1 - g_p) * 60;
	}
	else if(MAX3(r,g,b) == g)
	{
		if(MIN3(r,g,b) == b)
			h = (r_p + 1) * 60;
		else
			h = (3 - b_p) * 60;
	}
	else
	{
		if(MIN3(r,g,b) == r)
			h = (3 + g_p) * 60;
		else
			h = (5 - r_p) * 60;
	}
	
	
	*h_out = h + 0.5f;
	*s_out = (s * 255.0) + 0.5f;
	*v_out = v + 0.5f;
}


static void HSVfromRGB(PF_EventExtra *event_extra)
{
	RGBtoHSV(PICKER_DATA->r, PICKER_DATA->g, PICKER_DATA->b,
				&PICKER_DATA->h, &PICKER_DATA->s, &PICKER_DATA->v);
}


void HSVtoRGB(	unsigned short hue, unsigned short sat, unsigned short val,
						unsigned short *r_out, unsigned short *g_out, unsigned short *b_out )
{
	//hue 0-360, others 0-255

	int		pri;
	float	h = hue,
			s = sat / 255.0,
			v = val,
			hex, sec, a, b, c, R, G, B;
	
	hex = h / 60;
	pri = hex; // should be just the integer part
	sec = hex - pri;
	
	a = (1 - s) * v;
	b = (1 - (s * sec)) * v;
	c = (1 - (s * (1 - sec))) * v;
	
	switch(pri)
	{
		case 0: R = v; G = c; B = a; break;
		case 1: R = b; G = v; B = a; break;
		case 2: R = a; G = v; B = c; break;
		case 3: R = a; G = b; B = v; break;
		case 4: R = c; G = a; B = v; break;
		case 5: R = v; G = a; B = b; break;
		case 6: R = v; G = c; B = a; break;
	}
	
	*r_out = R + 0.5f;
	*g_out = G + 0.5f;
	*b_out = B + 0.5f;
}


static void RGBfromHSV(PF_EventExtra *event_extra)
{
	HSVtoRGB(PICKER_DATA->h, PICKER_DATA->s, PICKER_DATA->v,
				&PICKER_DATA->r, &PICKER_DATA->g, &PICKER_DATA->b);
}


static PF_Err SquareColorValue(PF_Pixel *output, int x_pos, int y_pos, PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	unsigned short r, g, b;
	
	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			output->red = PICKER_DATA->r;
			output->green = 255 - x_pos;
			output->blue = 255 - y_pos;
			break;
		
		case PICKER_GBR:
			output->red = 255 - y_pos;
			output->green = PICKER_DATA->g;
			output->blue = 255 - x_pos;
			break;

		case PICKER_BRG:
			output->red = 255 - x_pos;
			output->green = 255 - y_pos;
			output->blue = PICKER_DATA->b;
			break;

		case PICKER_HSV:
			HSVtoRGB(PICKER_DATA->h, 255 - x_pos, 255 - y_pos, &r, &g, &b);
			output->red = r;
			output->green = g;
			output->blue = b;
			break;

		case PICKER_SVH:
			HSVtoRGB((255.0 - (float)x_pos) * (360.0 / 255.0), PICKER_DATA->s, 255 - y_pos, &r, &g, &b);
			output->red = r;
			output->green = g;
			output->blue = b;
			break;

		case PICKER_VHS:
			HSVtoRGB((255.0 - (float)x_pos) * (360.0 / 255.0), 255 - y_pos, PICKER_DATA->v, &r, &g, &b);
			output->red = r;
			output->green = g;
			output->blue = b;
			break;

		default:
			output->red = 128;
			output->green = 128;
			output->blue = 128;
			break;
	}			
		
	output->alpha = 255;
	
	return err;
}

#if RECT_APPEARANCE == 1
#	define RED_RECT()		0
#	define GREEN_RECT()		0
#	define BLUE_RECT()		0
#elif RECT_APPEARANCE == 3
#	define RED_RECT()		(x_pos > 10)?0:PICKER_DATA->r
#	define GREEN_RECT()		(x_pos > 10)?0:PICKER_DATA->g
#	define BLUE_RECT()		(x_pos > 10)?0:PICKER_DATA->b
#else
#	define RED_RECT()		PICKER_DATA->r
#	define GREEN_RECT()		PICKER_DATA->g
#	define BLUE_RECT()		PICKER_DATA->b
#endif


static PF_Err RectColorValue(PF_Pixel *output, int x_pos, int y_pos, PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	unsigned short r, g, b;
	
	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			output->red = 255 - y_pos;
			output->green = GREEN_RECT();
			output->blue = BLUE_RECT();
			break;
		
		case PICKER_GBR:
			output->red = RED_RECT();
			output->green = 255 - y_pos;
			output->blue = BLUE_RECT();
			break;

		case PICKER_BRG:
			output->red = RED_RECT();
			output->green = GREEN_RECT();
			output->blue = 255 - y_pos;
			break;

		case PICKER_HSV:
			HSVtoRGB((short)((255.0 - (float)y_pos) * (360.0 / 255.0)), 255, 255, &r, &g, &b);
			output->red = (unsigned char)r;
			output->green = g;
			output->blue = b;
			break;

		case PICKER_SVH:
			HSVtoRGB(PICKER_DATA->h, 255 - y_pos, PICKER_DATA->v, &r, &g, &b);
			output->red = r;
			output->green = g;
			output->blue = b;
			break;

		case PICKER_VHS:
			HSVtoRGB(PICKER_DATA->h, PICKER_DATA->s, 255 - y_pos, &r, &g, &b);
			output->red = r;
			output->green = g;
			output->blue = b;
			break;

		default:
			output->red = 128;
			output->green = 128;
			output->blue = 128;
			break;
	}				
	
	output->alpha = 255;
	
	return err;
}

#define RGBAtoGRAB(PTR) \
	do { \
		unsigned char	tmp_r = (PTR)->red, \
						tmp_a = (PTR)->alpha; \
		(PTR)->red   = (PTR)->green; \
		(PTR)->green = tmp_r; \
		(PTR)->alpha = (PTR)->blue; \
		(PTR)->blue  = tmp_a; \
	} while (0);

static PF_Err DrawSquare(WIN_HDC_TYPE PF_Point *point, PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
#if USE_DRAWBOT
	PF_Handle buf_hndl;
	PF_Rect offscreenSize, offscreenRect;
	
	DRAWBOT_ColorRGBA line_color = {0.f, 0.f, 0.f, 1.f};
	
	suites.PFCustomUIThemeSuite()->PF_GetPreferredForegroundColor(&line_color);
	
	DRAWBOT_PenP  penP(suites.DBSupplierSuite(), supplier_ref, &line_color, LINE_THICKNESS);
	DRAWBOT_PathP pathP(suites.DBSupplierSuite(), supplier_ref);
#else // USE_DRAWBOT

#ifdef Macintosh
	CGrafPtr offscreenWorld; //screenWorld
	//BitMapPtr screenBitMapPtr;
	PixMapHandle offscreenPixMapHndl;
	//GDHandle deviceHandle;
	RGBColor fore, back;
	RGBColor white, black;
	Rect offscreenSize, offscreenRect;
#else
	PF_Handle			buf_hndl;
	//PF_Pixel		*offscreen;
	RECT			offscreenSize, offscreenRect;
	HDC				off_hdc;
	HBITMAP			bmap_hndl;
	BITMAPINFO		bmap_info;
	COLORREF		black_color = RGB(0,0,0);
	HBRUSH			black_brush	= CreateSolidBrush(black_color);
#endif
#endif // USE_DRAWBOT
	SInt32 offscreenRowbytes;
	Ptr offscreenPtr, rowPtr;
	PF_Pixel *placePixel;
	
	int i, j;
	

	//GetGWorld(&screenWorld, &deviceHandle);
	//screenBitMapPtr = GetPortBitMapForCopyBits(screenWorld);
	
	if(point == NULL)
	{
		offscreenSize.top = 0;
		offscreenSize.left = 0;
		offscreenSize.right = LITTLE_HALF(SQUARE_SIZE);
		offscreenSize.bottom = LITTLE_HALF(SQUARE_SIZE);
	}
	else
	{
		offscreenSize.top = MAX(0, point->v - RING_SIZE ); // tried (RING_SIZE + 1) to fix little redraw bug
		offscreenSize.left = MAX(0, point->h - RING_SIZE );
		offscreenSize.right = MIN(LITTLE_HALF(SQUARE_SIZE), point->h + RING_SIZE + 1);
		offscreenSize.bottom = MIN(LITTLE_HALF(SQUARE_SIZE), point->v + RING_SIZE + 1);
	}
	
	offscreenRect.top = offscreenSize.top + V_OFFSET + SQUARE_OFFSET;
	offscreenRect.left = offscreenSize.left + H_OFFSET + SQUARE_OFFSET;
	offscreenRect.right = offscreenRect.left + (offscreenSize.right - offscreenSize.left);
	offscreenRect.bottom = offscreenRect.top + (offscreenSize.bottom - offscreenSize.top);

#if USE_DRAWBOT
	offscreenRowbytes = (offscreenSize.right - offscreenSize.left) * 4;
	
	buf_hndl = PP_NEW_HANDLE(offscreenRowbytes * (offscreenSize.bottom - offscreenSize.top));
	
	offscreenPtr = (Ptr)PP_LOCK_HANDLE(buf_hndl);
#else // USE_DRAWBOT
#ifdef Macintosh
	NewGWorld(&offscreenWorld, 32, &offscreenSize, NULL, NULL, 0);
	
	offscreenPixMapHndl = GetGWorldPixMap(offscreenWorld);
	LockPixels(offscreenPixMapHndl);
	offscreenRowbytes = GetPixRowBytes(offscreenPixMapHndl);
	offscreenPtr = GetPixBaseAddr(offscreenPixMapHndl);
#else		
	bmap_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmap_info.bmiHeader.biHeight = offscreenSize.bottom - offscreenSize.top;
	bmap_info.bmiHeader.biWidth = offscreenSize.right - offscreenSize.left;
	bmap_info.bmiHeader.biPlanes = 1; //always this
	bmap_info.bmiHeader.biBitCount = 32;
	bmap_info.bmiHeader.biClrImportant = 0;
	bmap_info.bmiHeader.biCompression = BI_RGB;
	bmap_info.bmiHeader.biSizeImage = 0; //uncompressed
	bmap_info.bmiHeader.biClrUsed = 0;
	bmap_info.bmiHeader.biXPelsPerMeter = 1000;
	bmap_info.bmiHeader.biYPelsPerMeter = 1000;
	
	offscreenRowbytes = bmap_info.bmiHeader.biWidth * 4;
/*
	buf_hndl = HeapCreate(HEAP_NO_SERIALIZE,
							bmap_info.bmiHeader.biHeight * offscreenRowbytes,
							(bmap_info.bmiHeader.biHeight * offscreenRowbytes) + 10000);

	offscreenPtr = HeapAlloc(buf_hndl, HEAP_NO_SERIALIZE,
								bmap_info.bmiHeader.biHeight * offscreenRowbytes);
*/

	buf_hndl = PP_NEW_HANDLE(bmap_info.bmiHeader.biHeight * offscreenRowbytes);

	offscreenPtr = PP_LOCK_HANDLE(buf_hndl);
#endif
#endif // USE_DRAWBOT


	if(offscreenPtr)
	{
		rowPtr = offscreenPtr;
		placePixel = (PF_Pixel *)rowPtr;
		
#if defined(MSWindows) && !USE_DRAWBOT 
		for(j=offscreenSize.bottom; j> offscreenSize.top; j--)
#else
		for(j=offscreenSize.top; j< offscreenSize.bottom; j++)
#endif
		{
			for(i=offscreenSize.left; i< offscreenSize.right; i++)
			{
#ifdef MSWindows
				SquareColorValue(placePixel, LITTLE_DOUBLE(i), LITTLE_DOUBLE(j), event_extra);
				RGBAtoGRAB(placePixel);
				placePixel++;
#else
				SquareColorValue(placePixel++, LITTLE_DOUBLE(i), LITTLE_DOUBLE(j), event_extra);
#endif
			}
			
			rowPtr += offscreenRowbytes;
			placePixel = (PF_Pixel *)rowPtr;
		}

#if USE_DRAWBOT
		DRAWBOT_ImageRef imageP;
		
		suites.DBSupplierSuite()->NewImageFromBuffer(supplier_ref,
														(offscreenSize.right - offscreenSize.left),
														(offscreenSize.bottom - offscreenSize.top),
														offscreenRowbytes,
													#ifdef Macintosh
														kDRAWBOT_PixelLayout_32ARGB_Straight,
													#else
														kDRAWBOT_PixelLayout_32BGRA_Straight,
													#endif
														offscreenPtr,
														&imageP);
		
		DRAWBOT_PointF32 originDB = {0.5f + offscreenRect.left, 0.5f + offscreenRect.top};
		
		suites.DBSurfaceSuite()->DrawImage(surface_ref, imageP, &originDB, 1.f);
		
		suites.DBSupplierSuite()->ReleaseObject((DRAWBOT_ObjectRef)imageP);
		
		PP_DISPOSE_HANDLE(buf_hndl);
#else // USE_DRAWBOT
#ifdef Macintosh
		GetForeColor(&fore);
		GetBackColor(&back);
		
		white.red = white.green = white.blue = 0xFFFF;
		black.red = black.green = black.blue = 0;
		
		RGBForeColor(&black);
		RGBBackColor(&white);
		
	 	CopyBits( (BitMap*) *offscreenPixMapHndl,
#if TARGET_API_MAC_CARBON
				 GetPortBitMapForCopyBits((CGrafPtr)(**event_extra->contextH).cgrafptr),
#else
				&((GrafPtr)(**event_extra->contextH).cgrafptr)->portBits,
#endif
				 &offscreenSize,
				 &offscreenRect,
				 srcCopy,
				 NULL);
	
		UnlockPixels(offscreenPixMapHndl);
		
		DisposeGWorld(offscreenWorld);

#else

		off_hdc = CreateCompatibleDC(NULL);

		bmap_hndl = CreateDIBitmap(WIN_HDC &bmap_info.bmiHeader, CBM_INIT, offscreenPtr, &bmap_info, 
	          DIB_RGB_COLORS);
	
		SelectObject(off_hdc, bmap_hndl);
	
		BitBlt(ecw_hdc, offscreenRect.left, offscreenRect.top,
				bmap_info.bmiHeader.biWidth, bmap_info.bmiHeader.biHeight,
				off_hdc, 0, 0, SRCCOPY);
	
		DeleteDC(off_hdc);

		DeleteObject(bmap_hndl);
		
/*		HeapFree(buf_hndl, HEAP_NO_SERIALIZE, offscreenPtr);
	
		HeapDestroy(buf_hndl);
*/
		PP_DISPOSE_HANDLE(buf_hndl);
#endif
#endif // USE_DRAWBOT
	}

	// Draw the border
	
	if(point)
	{
		offscreenRect.top = V_OFFSET + SQUARE_OFFSET;
		offscreenRect.left = H_OFFSET + SQUARE_OFFSET;
		offscreenRect.right = offscreenRect.left + LITTLE_HALF(SQUARE_SIZE);
		offscreenRect.bottom = offscreenRect.top + LITTLE_HALF(SQUARE_SIZE);
	}
	
	offscreenRect.top -= 1;
	offscreenRect.left -= 1;
	offscreenRect.right += 1;
	offscreenRect.bottom += 1;

#if USE_DRAWBOT
	DRAWBOT_RectF32 offscreenRectDB = INIT_RECTF32(offscreenRect);
		
	suites.DBPathSuite()->AddRect(pathP, &offscreenRectDB);
	suites.DBSurfaceSuite()->StrokePath(surface_ref, penP, pathP);
#else
#ifdef Macintosh
	FrameRect(&offscreenRect);

	RGBForeColor(&fore);
	RGBBackColor(&back);
#else
	FrameRect(ecw_hdc, &offscreenRect, black_brush);
	DeleteObject(black_brush);
#endif

#endif // USE_DRAWBOT
	
	return err;
}


static PF_Err DrawRect(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
#if USE_DRAWBOT
	PF_Handle buf_hndl;
	PF_Rect offscreenSize, offscreenRect;
	
	DRAWBOT_ColorRGBA line_color = {0.f, 0.f, 0.f, 1.f};
	
	suites.PFCustomUIThemeSuite()->PF_GetPreferredForegroundColor(&line_color);
	
	DRAWBOT_PenP  penP(suites.DBSupplierSuite(), supplier_ref, &line_color, LINE_THICKNESS);
	DRAWBOT_PathP pathP(suites.DBSupplierSuite(), supplier_ref);
#else // USE_DRAWBOT

#ifdef Macintosh
	CGrafPtr offscreenWorld; //screenWorld
	//BitMapPtr screenBitMapPtr;
	PixMapHandle offscreenPixMapHndl;
	//GDHandle deviceHandle;
	RGBColor fore, back;
	RGBColor white, black;
	Rect offscreenSize, offscreenRect;
#else
	PF_Handle		buf_hndl;
	//PF_Pixel		*offscreen;
	RECT			offscreenSize, offscreenRect;
	HDC				off_hdc;
	HBITMAP			bmap_hndl;
	BITMAPINFO		bmap_info;
	COLORREF		black_color = RGB(0,0,0);
	HBRUSH			black_brush	= CreateSolidBrush(black_color);
#endif
#endif // USE_DRAWBOT

	SInt32 offscreenRowbytes;
	Ptr offscreenPtr, rowPtr;
	PF_Pixel *placePixel;
	
	int i, j;
	

	//GetGWorld(&screenWorld, &deviceHandle);
	//screenBitMapPtr = GetPortBitMapForCopyBits(screenWorld);
	
	offscreenSize.top = 0;
	offscreenSize.left = 0;
#if USE_DRAWBOT
	offscreenSize.right = LITTLE_HALF(RECT_WIDTH);
#else // USE_DRAWBOT
#ifdef Macintosh
	offscreenSize.right = 1;//RECT_WIDTH;
#else
	offscreenSize.right = LITTLE_HALF(RECT_WIDTH);
#endif
#endif // USE_DRAWBOT
	offscreenSize.bottom = LITTLE_HALF(SQUARE_SIZE);
	
	offscreenRect.top = V_OFFSET + SQUARE_OFFSET;
	offscreenRect.left = H_OFFSET + LITTLE_HALF(RECT_OFFSET);
	offscreenRect.right = offscreenRect.left + LITTLE_HALF(RECT_WIDTH); //now we draw 1 pixel wide and stretch it
	offscreenRect.bottom = offscreenRect.top + offscreenSize.bottom;

#if USE_DRAWBOT
	offscreenRowbytes = (offscreenSize.right - offscreenSize.left) * 4;
	
	buf_hndl = PP_NEW_HANDLE(offscreenRowbytes * (offscreenSize.bottom - offscreenSize.top));
	
	offscreenPtr = (Ptr)PP_LOCK_HANDLE(buf_hndl);
#else // USE_DRAWBOT
#ifdef Macintosh
	NewGWorld(&offscreenWorld, 32, &offscreenSize, NULL, NULL, 0);
	
	offscreenPixMapHndl = GetGWorldPixMap(offscreenWorld);
	LockPixels(offscreenPixMapHndl);
	offscreenRowbytes = GetPixRowBytes(offscreenPixMapHndl);
	offscreenPtr = GetPixBaseAddr(offscreenPixMapHndl);
#else
	bmap_info.bmiHeader.biSize = sizeof(BITMAPINFOHEADER);
	bmap_info.bmiHeader.biHeight = offscreenSize.bottom - offscreenSize.top;
	bmap_info.bmiHeader.biWidth = offscreenSize.right - offscreenSize.left;
	bmap_info.bmiHeader.biPlanes = 1; //always this
	bmap_info.bmiHeader.biBitCount = 32;
	bmap_info.bmiHeader.biClrImportant = 0;
	bmap_info.bmiHeader.biCompression = BI_RGB;
	bmap_info.bmiHeader.biSizeImage = 0; //uncompressed
	bmap_info.bmiHeader.biClrUsed = 0;
	bmap_info.bmiHeader.biXPelsPerMeter = 1000;
	bmap_info.bmiHeader.biYPelsPerMeter = 1000;
	
	offscreenRowbytes = bmap_info.bmiHeader.biWidth * 4;

	buf_hndl = PP_NEW_HANDLE(bmap_info.bmiHeader.biHeight * offscreenRowbytes);

	offscreenPtr = PP_LOCK_HANDLE(buf_hndl);
#endif
#endif // USE_DRAWBOT

	if(offscreenPtr)
	{
		rowPtr = offscreenPtr;
		
#if defined(MSWindows) && !USE_DRAWBOT 
		for(j=offscreenSize.bottom; j> 0; j--)
#else
		for(j=0; j< offscreenSize.bottom; j++)
#endif
		{
			placePixel = (PF_Pixel *)rowPtr;

			for(i=0; i< offscreenSize.right; i++)
			{
#ifdef MSWindows
				RectColorValue(placePixel, LITTLE_DOUBLE(i), LITTLE_DOUBLE(j), event_extra);
				RGBAtoGRAB(placePixel);
				placePixel++;
#else
				RectColorValue(placePixel++, LITTLE_DOUBLE(i), LITTLE_DOUBLE(j), event_extra);
#endif
			}
			
			rowPtr += offscreenRowbytes;
		}

#if USE_DRAWBOT
		DRAWBOT_ImageRef imageP;
		
		suites.DBSupplierSuite()->NewImageFromBuffer(supplier_ref,
														(offscreenSize.right - offscreenSize.left),
														(offscreenSize.bottom - offscreenSize.top),
														offscreenRowbytes,
													#ifdef Macintosh
														kDRAWBOT_PixelLayout_32ARGB_Straight,
													#else
														kDRAWBOT_PixelLayout_32BGRA_Straight,
													#endif
														offscreenPtr,
														&imageP);
		
		DRAWBOT_PointF32 originDB = {0.5f + offscreenRect.left, 0.5f + offscreenRect.top};
		
		suites.DBSurfaceSuite()->DrawImage(surface_ref, imageP, &originDB, 1.f);
		
		suites.DBSupplierSuite()->ReleaseObject((DRAWBOT_ObjectRef)imageP);
		
		PP_DISPOSE_HANDLE(buf_hndl);
#else // USE_DRAWBOT
#ifdef Macintosh
		GetForeColor(&fore);
		GetBackColor(&back);

		white.red = white.green = white.blue = 0xFFFF;
		black.red = black.green = black.blue = 0;
		
		RGBForeColor(&black);
		RGBBackColor(&white);
	
		CopyBits( (BitMap*) *offscreenPixMapHndl,
#if TARGET_API_MAC_CARBON
				GetPortBitMapForCopyBits((CGrafPtr)(**event_extra->contextH).cgrafptr),
#else
				&((GrafPtr)(**event_extra->contextH).cgrafptr)->portBits,
#endif
				 &offscreenSize,
				 &offscreenRect,
				 srcCopy,
				 NULL);
		
		UnlockPixels(offscreenPixMapHndl);
		
		DisposeGWorld(offscreenWorld);

#else

		off_hdc = CreateCompatibleDC(NULL);

		bmap_hndl = CreateDIBitmap(WIN_HDC &bmap_info.bmiHeader, CBM_INIT, offscreenPtr, &bmap_info, 
			  DIB_RGB_COLORS);

		SelectObject(off_hdc, bmap_hndl);

		BitBlt(ecw_hdc, offscreenRect.left, offscreenRect.top,
				bmap_info.bmiHeader.biWidth, bmap_info.bmiHeader.biHeight,
				off_hdc, 0, 0, SRCCOPY);

		DeleteDC(off_hdc);

		DeleteObject(bmap_hndl);

		PP_DISPOSE_HANDLE(buf_hndl);

#endif
#endif // USE_DRAWBOT
	}

	// Draw the border
	
	offscreenRect.top -= 1;
	offscreenRect.left -= 1;
	offscreenRect.right += 1;
	offscreenRect.bottom += 1;
	
#if USE_DRAWBOT
	DRAWBOT_RectF32 offscreenRectDB = INIT_RECTF32(offscreenRect);
		
	suites.DBPathSuite()->AddRect(pathP, &offscreenRectDB);
	suites.DBSurfaceSuite()->StrokePath(surface_ref, penP, pathP);
#else // USE_DRAWBOT
#ifdef Macintosh
	FrameRect(&offscreenRect);

	RGBForeColor(&fore);
	RGBBackColor(&back);
#else
	FrameRect(ecw_hdc, &offscreenRect, black_brush);
	DeleteObject(black_brush);
#endif
#endif //USE_DRAWBOT


	return err;
}


static unsigned char Xcoord(PF_EventExtra *event_extra)
{
	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			return (255 - PICKER_DATA->g);
		
		case PICKER_GBR:
			return (255 - PICKER_DATA->b);

		case PICKER_BRG:
			return (255 - PICKER_DATA->r);

		case PICKER_HSV:
			return (255 - PICKER_DATA->s);

		case PICKER_SVH:
			return (unsigned char)((255.0 - ((float)PICKER_DATA->h * (255.0 / 360.0))));

		case PICKER_VHS:
			return (unsigned char)((255.0 - ((float)PICKER_DATA->h * (255.0 / 360.0))));

		default:
			return 128;
	}			
}

static unsigned char Ycoord(PF_EventExtra *event_extra)
{
	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			return (255 - PICKER_DATA->b);
		
		case PICKER_GBR:
			return (255 - PICKER_DATA->r);

		case PICKER_BRG:
			return (255 - PICKER_DATA->g);

		case PICKER_HSV:
			return (255 - PICKER_DATA->v);

		case PICKER_SVH:
			return (255 - PICKER_DATA->v);

		case PICKER_VHS:
			return (255 - PICKER_DATA->s);

		default:
			return 128;
	}			
}

static unsigned char Zcoord(PF_EventExtra *event_extra)
{
	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			return (255 - PICKER_DATA->r);
		
		case PICKER_GBR:
			return (255 - PICKER_DATA->g);

		case PICKER_BRG:
			return (255 - PICKER_DATA->b);

		case PICKER_HSV:
			return (unsigned char)((255.0 - ((float)PICKER_DATA->h * (255.0 / 360.0))));

		case PICKER_SVH:
			return (255 - PICKER_DATA->s);

		case PICKER_VHS:
			return (255 - PICKER_DATA->v);

		default:
			return 128;
	}			
}


static PF_Err DrawUpArrow(WIN_HDC_TYPE SInt16 h, SInt16 v)
{
	PF_Err err = PF_Err_NONE;

#if USE_DRAWBOT
	DRAWBOT_ColorRGBA line_color = {0.f, 0.f, 0.f, 1.f};
	
	suites.PFCustomUIThemeSuite()->PF_GetPreferredForegroundColor(&line_color);
	
	DRAWBOT_PenP  penP(suites.DBSupplierSuite(), supplier_ref, &line_color, LINE_THICKNESS);
	DRAWBOT_PathP pathP(suites.DBSupplierSuite(), supplier_ref);
	
	suites.DBPathSuite()->MoveTo(pathP, 0.5f + h, 1.5f + v);
	suites.DBPathSuite()->LineTo(pathP, 0.5f + h - ARROW_SIZE, 1.5f + v + ARROW_SIZE);
	suites.DBPathSuite()->LineTo(pathP, 0.5f + h + ARROW_SIZE, 1.5f + v + ARROW_SIZE);
	suites.DBPathSuite()->LineTo(pathP, 0.5f + h, 1.5f + v);
	
	suites.DBSurfaceSuite()->StrokePath(surface_ref, penP, pathP);
#else // USE_DRAWBOT

#if MSWindows
	MoveToEx(WIN_HDC h, v, NULL);
#else
	MoveTo(WIN_HDC h, v);
#endif
	LineTo(WIN_HDC h - ARROW_SIZE, v + ARROW_SIZE);
	LineTo(WIN_HDC h + ARROW_SIZE, v + ARROW_SIZE);
	LineTo(WIN_HDC h, v);
	
#endif // USE_DRAWBOT
	
	return err;
}

static PF_Err EraseUpArrow(WIN_HDC_TYPE SInt16 h, SInt16 v)
{
	PF_Err err = PF_Err_NONE;	

#if USE_DRAWBOT

#else // USE_DRAWBOT

#ifdef Macintosh
	PF_Rect r;
#else
	RECT			r;
	HBRUSH			brush = CreateSolidBrush( GetBkColor(ecw_hdc) );
#endif

	r.top = v;
	r.left = h - ARROW_SIZE;
	r.bottom = v + ARROW_SIZE + 1;
	r.right = h + ARROW_SIZE + 1;
		
#ifdef Macintosh
	EraseRect(&r);
#else
	FillRect(WIN_HDC &r, brush);
	DeleteObject(brush);
#endif

#endif // USE_DRAWBOT
		
	return err;
}



static PF_Err DrawLeftArrow(WIN_HDC_TYPE SInt16 h, SInt16 v)
{
	PF_Err err = PF_Err_NONE;

#if USE_DRAWBOT
	DRAWBOT_ColorRGBA line_color = {0.f, 0.f, 0.f, 1.f};
	
	suites.PFCustomUIThemeSuite()->PF_GetPreferredForegroundColor(&line_color);
	
	DRAWBOT_PenP  penP(suites.DBSupplierSuite(), supplier_ref, &line_color, LINE_THICKNESS);
	DRAWBOT_PathP pathP(suites.DBSupplierSuite(), supplier_ref);
	
	suites.DBPathSuite()->MoveTo(pathP, 1.5f + h, 0.5f + v);
	suites.DBPathSuite()->LineTo(pathP, 1.5f + h + ARROW_SIZE, 0.5f + v - ARROW_SIZE);
	suites.DBPathSuite()->LineTo(pathP, 1.5f + h + ARROW_SIZE, 0.5f + v + ARROW_SIZE);
	suites.DBPathSuite()->LineTo(pathP, 1.5f + h, 0.5f + v);
	
	suites.DBSurfaceSuite()->StrokePath(surface_ref, penP, pathP);
#else // USE_DRAWBOT

#if MSWindows
	MoveToEx(WIN_HDC h, v, NULL);
#else
	MoveTo(WIN_HDC h, v);
#endif
	LineTo(WIN_HDC h + ARROW_SIZE, v - ARROW_SIZE);
	LineTo(WIN_HDC h + ARROW_SIZE, v + ARROW_SIZE);
	LineTo(WIN_HDC h, v);
	
#endif // USE_DRAWBOT

	return err;
}

static PF_Err EraseLeftArrow(WIN_HDC_TYPE SInt16 h, SInt16 v)
{
	PF_Err err = PF_Err_NONE;	

#if USE_DRAWBOT

#else // USE_DRAWBOT

#ifdef Macintosh
	PF_Rect r;
#else
	RECT			r;
	HBRUSH			brush = CreateSolidBrush( GetBkColor(ecw_hdc) );
#endif

	r.top = v - ARROW_SIZE;
	r.left = h;
	r.bottom = v + ARROW_SIZE + 1;
	r.right = h + ARROW_SIZE + 1;
		
#ifdef Macintosh
	EraseRect(&r);
#else
	FillRect(WIN_HDC &r, brush);
	DeleteObject(brush);
#endif

#endif // USE_DRAWBOT

	return err;
}



static PF_Err Ring(WIN_HDC_TYPE SInt16 h, SInt16 v)
{
	PF_Err err = PF_Err_NONE;

#if USE_DRAWBOT
	// shouldn't actually get here, see DrawRing()
#else // USE_DRAWBOT

#ifdef Macintosh
	Rect r;
	
	r.top = v - RING_SIZE;
	r.left = h - RING_SIZE;
	r.bottom = v + RING_SIZE + 1;
	r.right = h + RING_SIZE + 1;

	FrameOval(&r);
#else
	//MoveToEx(WIN_HDC h, v, NULL);
	Arc(ecw_hdc, h-RING_SIZE, v-RING_SIZE, h+RING_SIZE+1, v+RING_SIZE+1, h-RING_SIZE, v, h-RING_SIZE, v);
#endif

#endif // USE_DRAWBOT

	return err;
}

static PF_Boolean ColorIsDark(PF_EventExtra *event_extra)
{
	return ( 	(0.299 * (float)PICKER_DATA->r) +
				(0.587 * (float)PICKER_DATA->g) +
				(0.114 * (float)PICKER_DATA->b) < 128 );
}


static PF_Err DrawRing(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
#if USE_DRAWBOT
	DRAWBOT_ColorRGBA ring_color = {0.f, 0.f, 0.f, 1.f};
	
	if( ColorIsDark(event_extra) )
		ring_color.red = ring_color.green = ring_color.blue = (float)RING_SHADE / 255.0;
	else
		ring_color.red = ring_color.green = ring_color.blue = (float)(255 - RING_SHADE) / 255.0;
	
	DRAWBOT_PointF32 center;
	center.x = 0.5f + H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Xcoord(event_extra));
	center.y = 0.5f + V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Ycoord(event_extra));
	
	DRAWBOT_PenP  penP(suites.DBSupplierSuite(), supplier_ref, &ring_color, LINE_THICKNESS);
	DRAWBOT_PathP pathP(suites.DBSupplierSuite(), supplier_ref);
	
	suites.DBPathSuite()->AddArc(pathP, &center, RING_SIZE, 0.f, 360.f);
	
	suites.DBSurfaceSuite()->StrokePath(surface_ref, penP, pathP);
#else // USE_DRAWBOT

#ifdef Macintosh
	RGBColor fore, ring;
	
	GetForeColor(&fore);
	
	if( ColorIsDark(event_extra) )
		ring.red = ring.green = ring.blue = RING_SHADE << 8;
	else
		ring.red = ring.green = ring.blue = (255 - RING_SHADE) << 8;
	
	RGBForeColor(&ring);

	Ring(H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Xcoord(event_extra)), V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Ycoord(event_extra)) );

	RGBForeColor(&fore);
#else
	HPEN	old_pen = NULL,
			dark_pen  = CreatePen(PS_SOLID, 1, RGB(255-RING_SHADE, 255-RING_SHADE, 255-RING_SHADE) ),
			light_pen = CreatePen(PS_SOLID, 1, RGB(RING_SHADE, RING_SHADE, RING_SHADE) );

	old_pen = GetCurrentObject(WIN_HDC OBJ_PEN);

	if( ColorIsDark(event_extra) )
		SelectObject(WIN_HDC light_pen);
	else
		SelectObject(WIN_HDC dark_pen);

	Ring(WIN_HDC H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Xcoord(event_extra)), V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Ycoord(event_extra)) );

	if(old_pen)
		SelectObject(WIN_HDC old_pen);
	
	if(dark_pen)
		DeleteObject(dark_pen);

	if(light_pen)
		DeleteObject(light_pen);
#endif
	
#endif // USE_DRAWBOT
	
	return err;
}


static PF_Err DrawRectArrow(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	DrawLeftArrow(WIN_HDC H_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + 1, V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Zcoord(event_extra)) );
	
	return err;
}

static PF_Err EraseRectArrow(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	EraseLeftArrow(WIN_HDC H_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + 1, V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Zcoord(event_extra)) );
	
	return err;
}

static PF_Err DrawSquareArrowV(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	DrawLeftArrow(WIN_HDC H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) + 1, V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Ycoord(event_extra)) );
	
	return err;
}

static PF_Err EraseSquareArrowV(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	EraseLeftArrow(WIN_HDC H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) + 1, V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Ycoord(event_extra)) );
	
	return err;
}

static PF_Err DrawSquareArrowH(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	DrawUpArrow(WIN_HDC H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Xcoord(event_extra)), V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) + 1);
	
	return err;
}

static PF_Err EraseSquareArrowH(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	EraseUpArrow(WIN_HDC H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Xcoord(event_extra)), V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) + 1);
	
	return err;
}


static PF_Err DrawArrows(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	DrawRectArrow(WIN_HDC event_extra);	
	DrawSquareArrowV(WIN_HDC event_extra);	
	DrawSquareArrowH(WIN_HDC event_extra);
	
	return err;
}

static PF_Err EraseArrows(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	EraseRectArrow(WIN_HDC event_extra);	
	EraseSquareArrowV(WIN_HDC event_extra);	
	EraseSquareArrowH(WIN_HDC event_extra);
	
	return err;
}


static PF_Err EraseRing(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
	PF_Point loc;
	
	loc.h = LITTLE_HALF(Xcoord(event_extra));
	loc.v = LITTLE_HALF(Ycoord(event_extra));


	DrawSquare(WIN_HDC &loc, event_extra);
	
	if( LITTLE_HALF(Xcoord(event_extra)) < RING_SIZE )
	{
		EraseLeftArrow(WIN_HDC H_OFFSET + SQUARE_OFFSET - (ARROW_SIZE + 2), V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Ycoord(event_extra)) );
	}
/*	else if ( Xcoord(event_extra) > (255 - RING_SIZE) )
	{
		EraseSquareArrowV(event_extra);
		DrawSquareArrowV(event_extra);
	}
*/	
	if( LITTLE_HALF(Ycoord(event_extra)) < RING_SIZE )
	{
		EraseUpArrow(WIN_HDC H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(Xcoord(event_extra)), V_OFFSET + SQUARE_OFFSET - (ARROW_SIZE + 2) );
	}
/*	else if( Ycoord(event_extra) > (255 - RING_SIZE) ) 
	{
		EraseSquareArrowH(event_extra);
		DrawSquareArrowH(event_extra);
	}
*/	
	return err;
}

#if USE_DRAWBOT
#define DRAW_STRING(STRING) \
	do{ \
		DRAWBOT_UTF16Char u_str[10]; \
		char *c = STRING; \
		DRAWBOT_UTF16Char *u = u_str; \
		if(*c != '\0'){ do{ *u++ = *c++; }while(*c != '\0'); } \
		*u = '\0'; \
		suites.DBSurfaceSuite()->DrawString(surface_ref, brushP.Get(), fontP.Get(), u_str, \
				&text_origin, kDRAWBOT_TextAlignment_Right, kDRAWBOT_TextTruncation_None, 0.0f); \
	}while(0);
#define DRAW_NUM(NUM) \
	do{ \
		char buf[8]; \
		sprintf(buf, "%d", (NUM)); \
		DRAW_STRING(buf); \
	} while(0);
#else // USE_DRAWBOT
#ifdef Macintosh
#define DRAW_STRING(STRING)	DrawText(STRING, 0, strlen(STRING));
#define DRAW_NUM(NUM) \
	do{\
		char buf[8];\
		int len;\
		sprintf(buf, "%d", (NUM) );\
		len = TextWidth(buf, 0, strlen(buf));\
		Move(-len, 0);\
		DrawText(buf, 0, strlen(buf));\
		Move(len, 0);\
	} while(0)
#else
#define DRAW_STRING(STRING) \
	do{\
		UINT old_align = GetTextAlign(ecw_hdc); \
		SetTextAlign(ecw_hdc, TA_RIGHT); \
		TextOut(ecw_hdc, pen.h + 6, pen.v - 10, (STRING), strlen(STRING)); \
		SetTextAlign(ecw_hdc, old_align); \
	} while(0)
#define DRAW_NUM(NUM) \
	do{\
		char buf[8];\
		int len;\
		UINT old_align = GetTextAlign(ecw_hdc); \
		sprintf(buf, "%d", (NUM) );\
		SetTextAlign(ecw_hdc, TA_RIGHT); \
		TextOut(ecw_hdc, pen.h, pen.v - 10, buf, strlen(buf)); \
		SetTextAlign(ecw_hdc, old_align); \
	} while(0)
#endif

#endif // USE_DRAWBOT


enum {
	TEXT_R = 0,
	TEXT_G,
	TEXT_B,
	TEXT_H,
	TEXT_S,
	TEXT_V,
	TEXT_NUM
};

static PF_Err DrawTextBoxes(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;

#if USE_DRAWBOT
	PF_Rect box, big_box;
	PF_Point pen;

	float default_font_sizeF = 0.0;
	suites.DBSupplierSuite()->GetDefaultFontSize(supplier_ref, &default_font_sizeF);
	
	DRAWBOT_ColorRGBA line_color = {0.f, 0.f, 0.f, 1.f};
	suites.PFCustomUIThemeSuite()->PF_GetPreferredForegroundColor(&line_color);
	
	DRAWBOT_PointF32 text_origin = {0.f, 0.f};

	DRAWBOT_FontP fontP(suites.DBSupplierSuite(), supplier_ref, default_font_sizeF);
	DRAWBOT_BrushP brushP(suites.DBSupplierSuite(), supplier_ref, &line_color);
	DRAWBOT_PenP penP(suites.DBSupplierSuite(), supplier_ref, &line_color, LINE_THICKNESS);
#else // USE_DRAWBOT

#ifdef Macintosh	
	PF_Rect box, big_box;
	PF_Point pen;
	RGBColor back, hilite_color;
#else
	RECT box, big_box;
	PF_Point	pen;
	COLORREF	background_color = GetBkColor(ecw_hdc),
				hilite_color = RGB(GetRValue(background_color)-TEXT_BOX_HILITE_DELTA, GetGValue(background_color)-TEXT_BOX_HILITE_DELTA, GetBValue(background_color));
	HBRUSH		black_brush	= CreateSolidBrush( RGB(0,0,0) ),
				background_brush = CreateSolidBrush( background_color ),
				hilite_brush = CreateSolidBrush( hilite_color );
#endif

#endif // USE_DRAWBOT
	
	char *box_txt[] = {"R", "G", "B", "H", "S", "V"};
	
	int i;
	
	if(LITTLE_UI)
	{
		box.top =  V_OFFSET + SQUARE_OFFSET;
		box.left = H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP;
		box.bottom = box.top + TEXT_BOX_HEIGHT;
		box.right = box.left + TEXT_BOX_WIDTH;
		
		big_box.top = box.top - TEXT_BOX_HILITE_UEXT;
		big_box.left = box.left - (TEXT_LABEL_HSHIFT + TEXT_BOX_HILITE_LEXT);
		big_box.bottom = box.bottom + TEXT_BOX_HILITE_DEXT + ((PICKERS_TOTAL-1) * TEXT_LITTLE_STEP);
		big_box.right = box.right + TEXT_BOX_HILITE_REXT;
	}
	else
	{
		box.top =  V_OFFSET + TEXT_OFFSET;
		box.left = H_OFFSET + SQUARE_OFFSET + TEXT_BOX_HSHIFT;
		box.bottom = box.top + TEXT_BOX_HEIGHT;
		box.right = box.left + TEXT_BOX_WIDTH;
		
		big_box.top = box.top - TEXT_BOX_HILITE_UEXT;
		big_box.left = box.left - (TEXT_LABEL_HSHIFT + TEXT_BOX_HILITE_LEXT);
		big_box.bottom = box.bottom + TEXT_BOX_HILITE_DEXT;
		big_box.right = box.right + ((PICKERS_TOTAL-1) * TEXT_STEP) + TEXT_BOX_HILITE_REXT;
	}

#if USE_DRAWBOT

#else // USE_DRAWBOT
#ifdef Macintosh
	EraseRect(&big_box);
#else
	FillRect(WIN_HDC &big_box, background_brush);
#endif
#endif // USE_DRAWBOT
	
	pen.h = box.left - TEXT_LABEL_HSHIFT;
	pen.v = box.bottom - TEXT_LABEL_VSHIFT;
	
	for(i=0; i< 6; i++)
	{
		if(i == PICKER_DATA->which)
		{
			big_box.top = box.top - TEXT_BOX_HILITE_UEXT + (LITTLE_UI?1:0);
			big_box.left = box.left - (TEXT_LABEL_HSHIFT + TEXT_BOX_HILITE_LEXT) + (LITTLE_UI?1:0);
			big_box.bottom = box.bottom + TEXT_BOX_HILITE_DEXT - (LITTLE_UI?1:0);
			big_box.right = box.right + TEXT_BOX_HILITE_REXT - (LITTLE_UI?1:0);
			
		#if USE_DRAWBOT
			DRAWBOT_ColorRGBA hilight_color = {0.f, 0.f, 0.3f, 0.5f};
			DRAWBOT_RectF32 big_boxDB = INIT_RECTF32(big_box);
			
			suites.DBSurfaceSuite()->PaintRect(surface_ref, &hilight_color, &big_boxDB);
		#else // USE_DRAWBOT
		#ifdef Macintosh
			GetBackColor(&back);
			hilite_color.red   = back.red   - (TEXT_BOX_HILITE_DELTA << 8);
			hilite_color.green = back.green - (TEXT_BOX_HILITE_DELTA << 8);
			hilite_color.blue  = back.blue;
			RGBBackColor(&hilite_color);
			
			EraseRect(&big_box);
		#else
			SetBkColor(WIN_HDC hilite_color);
			FillRect(WIN_HDC &big_box, hilite_brush);
		#endif
		#endif // USE_DRAWBOT
		}

#if USE_DRAWBOT
		DRAWBOT_PathP pathP(suites.DBSupplierSuite(), supplier_ref);
		
		DRAWBOT_RectF32 boxDB = INIT_RECTF32(box);
		
		suites.DBPathSuite()->AddRect(pathP, &boxDB);
		suites.DBSurfaceSuite()->StrokePath(surface_ref, penP, pathP);
		
		text_origin.x = 0.5f + pen.h;
		text_origin.y = 0.5f + pen.v;
#else // USE_DRAWBOT		
#ifdef Macintosh
		FrameRect(&box);
		
		MoveTo(pen.h, pen.v);
#else
		FrameRect(WIN_HDC &box, black_brush);
#endif
#endif // USE_DRAWBOT
		

		DRAW_STRING( box_txt[i] );
		
		
		if(LITTLE_UI)
		{
			box.top += TEXT_LITTLE_STEP;
			box.bottom += TEXT_LITTLE_STEP;
			
			pen.v += TEXT_LITTLE_STEP;
		}
		else
		{
			box.left += TEXT_STEP;
			box.right += TEXT_STEP;
			
			pen.h += TEXT_STEP;
		}
		
#if USE_DRAWBOT

#else // USE_DRAWBOT
		if(i == PICKER_DATA->which)
	#ifdef Macintosh
			RGBBackColor(&back);
	#else
			SetBkColor(WIN_HDC background_color);
	#endif
#endif // USE_DRAWBOT
	}
	
#if USE_DRAWBOT

#else // USE_DRAWBOT
#ifndef Macintosh
	DeleteObject(black_brush);
	DeleteObject(background_brush);
	DeleteObject(hilite_brush);
#endif
#endif // USE_DRAWBOT

	return err;
}


static PF_Err DrawTextBorder(WIN_HDC_TYPE int which, PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	
#if USE_DRAWBOT
	PF_Rect border;
#else // USE_DRAWBOT
#ifdef Macintosh	
	PF_Rect border;
	RGBColor fore, back, border_color;
#else
	RECT border;
	HBRUSH border_brush = CreateSolidBrush( RGB(GetRValue(GetBkColor(ecw_hdc)),GetGValue(GetBkColor(ecw_hdc))-TEXT_BOX_HILITE_DELTA,GetBValue(GetBkColor(ecw_hdc))-TEXT_BOX_HILITE_DELTA) );
#endif
#endif // USE_DRAWBOT
	
	if(LITTLE_UI)
	{
		border.top =  V_OFFSET + SQUARE_OFFSET - TEXT_BOX_BORDER_UEXT + (which * TEXT_LITTLE_STEP);
		border.left = H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP - (TEXT_LABEL_HSHIFT + TEXT_BOX_BORDER_LEXT);
		border.bottom = V_OFFSET + SQUARE_OFFSET + TEXT_BOX_HEIGHT + TEXT_BOX_BORDER_DEXT + (which * TEXT_LITTLE_STEP);
		border.right = H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP + TEXT_BOX_WIDTH + TEXT_BOX_BORDER_REXT;
	}
	else
	{
		border.top = V_OFFSET + TEXT_OFFSET - TEXT_BOX_BORDER_UEXT;
		border.left = H_OFFSET + SQUARE_OFFSET + TEXT_BOX_HSHIFT - (TEXT_LABEL_HSHIFT + TEXT_BOX_BORDER_LEXT) + (which * TEXT_STEP);
		border.bottom = V_OFFSET + TEXT_OFFSET + TEXT_BOX_HEIGHT + TEXT_BOX_BORDER_DEXT ;
		border.right = H_OFFSET + SQUARE_OFFSET + TEXT_BOX_HSHIFT + TEXT_BOX_WIDTH + (which * TEXT_STEP) + TEXT_BOX_BORDER_REXT;
	}

#if USE_DRAWBOT

#else // USE_DRAWBOT
#ifdef Macintosh
	GetForeColor(&fore);
	GetBackColor(&back);
	border_color.red   = back.red;
	border_color.green = back.green - (TEXT_BOX_BORDER_DELTA << 8);
	border_color.blue  = back.blue  - (TEXT_BOX_BORDER_DELTA << 8);
	RGBForeColor(&border_color);
	
	FrameRect(&border);
	
	RGBForeColor(&fore);
#else
	FrameRect(WIN_HDC &border, border_brush);
	DeleteObject(border_brush);
#endif
#endif // USE_DRAWBOT

	return err;
}


static void HiliteChanges(WIN_HDC_TYPE int type, PF_EventExtra *event_extra)
{
#define BORDER(NUM) DrawTextBorder(WIN_HDC (NUM), event_extra)

	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			switch(type)
			{	case CLICK_SQUARE: 			BORDER(TEXT_G); BORDER(TEXT_B); break;
				case CLICK_RECT:			BORDER(TEXT_R); break;
				case CLICK_SQUARE_H_ARROW:	BORDER(TEXT_G); break;
				case CLICK_SQUARE_V_ARROW:	BORDER(TEXT_B); break; }
			break;
		
		case PICKER_GBR:
			switch(type)
			{	case CLICK_SQUARE: 			BORDER(TEXT_B); BORDER(TEXT_R); break;
				case CLICK_RECT:			BORDER(TEXT_G); break;
				case CLICK_SQUARE_H_ARROW:	BORDER(TEXT_B); break;
				case CLICK_SQUARE_V_ARROW:	BORDER(TEXT_R); break; }
			break;

		case PICKER_BRG:
			switch(type)
			{	case CLICK_SQUARE: 			BORDER(TEXT_R); BORDER(TEXT_G); break;
				case CLICK_RECT:			BORDER(TEXT_B); break;
				case CLICK_SQUARE_H_ARROW:	BORDER(TEXT_R); break;
				case CLICK_SQUARE_V_ARROW:	BORDER(TEXT_G); break; }
			break;

		case PICKER_HSV:
			switch(type)
			{	case CLICK_SQUARE: 			BORDER(TEXT_S); BORDER(TEXT_V); break;
				case CLICK_RECT:			BORDER(TEXT_H); break;
				case CLICK_SQUARE_H_ARROW:	BORDER(TEXT_S); break;
				case CLICK_SQUARE_V_ARROW:	BORDER(TEXT_V); break; }
			break;

		case PICKER_SVH:
			switch(type)
			{	case CLICK_SQUARE: 			BORDER(TEXT_V); BORDER(TEXT_H); break;
				case CLICK_RECT:			BORDER(TEXT_S); break;
				case CLICK_SQUARE_H_ARROW:	BORDER(TEXT_H); break;
				case CLICK_SQUARE_V_ARROW:	BORDER(TEXT_V); break; }
			break;

		case PICKER_VHS:
			switch(type)
			{	case CLICK_SQUARE: 			BORDER(TEXT_H); BORDER(TEXT_S); break;
				case CLICK_RECT:			BORDER(TEXT_V); break;
				case CLICK_SQUARE_H_ARROW:	BORDER(TEXT_H); break;
				case CLICK_SQUARE_V_ARROW:	BORDER(TEXT_S); break; }
			break;
	}			
}



static PF_Err DrawTextValues(WIN_HDC_TYPE PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	unsigned short values[6];
	
	int i;
	
	PF_Point pen;
	
#if USE_DRAWBOT
	PF_Rect box;

	float default_font_sizeF = 0.0;
	suites.DBSupplierSuite()->GetDefaultFontSize(supplier_ref, &default_font_sizeF);
	
	DRAWBOT_ColorRGBA line_color = {0.f, 0.f, 0.f, 1.f};
	suites.PFCustomUIThemeSuite()->PF_GetPreferredForegroundColor(&line_color);
	
	DRAWBOT_PointF32 text_origin = {0.f, 0.f};

	DRAWBOT_FontP fontP(suites.DBSupplierSuite(), supplier_ref, default_font_sizeF);
	DRAWBOT_BrushP brushP(suites.DBSupplierSuite(), supplier_ref, &line_color);
	DRAWBOT_PenP penP(suites.DBSupplierSuite(), supplier_ref, &line_color, LINE_THICKNESS);
#else // USE_DRAWBOT
#ifdef Macintosh
	PF_Rect box;
#else
	RECT box;
	HBRUSH background_brush = CreateSolidBrush( GetBkColor(ecw_hdc) );
#endif
#endif // USE_DRAWBOT
	
	if(LITTLE_UI)
	{
		box.top =  V_OFFSET + SQUARE_OFFSET + 1;
		box.left = H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP + 1;
		box.bottom = box.top + TEXT_BOX_HEIGHT - 2;
		box.right = box.left + TEXT_BOX_WIDTH - 2;

		pen.h = H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) + ARROW_SIZE + TEXT_LITTLE_GAP + TEXT_BOX_WIDTH - TEXT_VALUE_HSHIFT;
		pen.v = V_OFFSET + SQUARE_OFFSET + TEXT_BOX_HEIGHT - TEXT_VALUE_VSHIFT;
	}
	else
	{
		box.top =  V_OFFSET + TEXT_OFFSET + 1;
		box.left = H_OFFSET + SQUARE_OFFSET + TEXT_BOX_HSHIFT + 1;
		box.bottom = box.top + TEXT_BOX_HEIGHT - 2;
		box.right = box.left + TEXT_BOX_WIDTH - 2;

		pen.h = H_OFFSET + TEXT_BOX_HSHIFT + TEXT_BOX_WIDTH - TEXT_VALUE_HSHIFT;
		pen.v = V_OFFSET + TEXT_OFFSET + TEXT_BOX_HEIGHT - TEXT_VALUE_VSHIFT;
	}
	
	
	values[0] = PICKER_DATA->r;
	values[1] = PICKER_DATA->g;
	values[2] = PICKER_DATA->b;
	values[3] = PICKER_DATA->h;
	values[4] = PICKER_DATA->s;
	values[5] = PICKER_DATA->v;

	for(i=0; i< 6; i++)
	{
#if USE_DRAWBOT
		text_origin.x = 0.5f + pen.h;
		text_origin.y = 0.5f + pen.v;
#else // USE_DRAWBOT
#ifdef Macintosh
		MoveTo(pen.h, pen.v);
		EraseRect(&box);
#else
		FillRect(WIN_HDC &box, background_brush);
#endif
#endif // USE_DRAWBOT
		DRAW_NUM(values[i]);
		
		if(LITTLE_UI)
		{
			pen.v += TEXT_LITTLE_STEP;
			box.top += TEXT_LITTLE_STEP;
			box.bottom += TEXT_LITTLE_STEP;
		}
		else
		{
			pen.h += TEXT_STEP;
			box.left += TEXT_STEP;
			box.right += TEXT_STEP;
		}
	}

#if USE_DRAWBOT

#else // USE_DRAWBOT
#ifndef Macintosh
	DeleteObject(background_brush);
#endif
#endif // USE_DRAWBOT

	return err;
}



static PF_Err
DrawEvent(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err			err		=	PF_Err_NONE;
	long			width = 0, height = 0;
	//void*			dp = (*(event_extra->contextH))->cgrafptr;
	
#if USE_DRAWBOT
	AEGP_SuiteHandler suites(in_data->pica_basicP);
	
	DRAWBOT_DrawRef			drawbot_ref = NULL;
	suites.PFEffectCustomUISuite()->PF_GetDrawingReference(event_extra->contextH, &drawbot_ref);
	
	DRAWBOT_SupplierRef		supplier_ref = NULL;
	DRAWBOT_SurfaceRef		surface_ref = NULL;

	suites.DBDrawbotSuite()->GetSupplier(drawbot_ref, &supplier_ref);
	suites.DBDrawbotSuite()->GetSurface(drawbot_ref, &surface_ref);
#else // USE_DRAWBOT
#if MSWindows
	void*			dp = (*(event_extra->contextH))->cgrafptr;
	HDC				ecw_hdc	=	0;
	PF_GET_CGRAF_DATA(dp, PF_CGrafData_HDC, &ecw_hdc);
#endif
#endif // USE_DRAWBOT

	event_extra->evt_out_flags = PF_EO_NONE;

	if (	!(event_extra->evt_in_flags & PF_EI_DONT_DRAW) && 
			PF_EA_CONTROL == event_extra->effect_win.area)
	{
#if USE_HSV
		if(	PICKER_DATA->h != params[FILTER_HUE]->u.sd.value ||
			PICKER_DATA->s != params[FILTER_SAT]->u.sd.value ||
			PICKER_DATA->v != params[FILTER_VAL]->u.sd.value  )
		{
			EraseRing(WIN_HDC event_extra);

			PICKER_DATA->h = params[FILTER_HUE]->u.sd.value;
			PICKER_DATA->s = params[FILTER_SAT]->u.sd.value;
			PICKER_DATA->v = params[FILTER_VAL]->u.sd.value;
						
			HSVtoRGB(PICKER_DATA->h, PICKER_DATA->s, PICKER_DATA->v,
						&PICKER_DATA->r, &PICKER_DATA->g, &PICKER_DATA->b);
			
			DrawRing(WIN_HDC event_extra);
			
			UPDATE_HSV_SLIDERS( PICKER_DATA );
		}
#else
		if(	PICKER_DATA->r != params[FILTER_COLOR]->u.cd.value.red ||
			PICKER_DATA->g != params[FILTER_COLOR]->u.cd.value.green ||
			PICKER_DATA->b != params[FILTER_COLOR]->u.cd.value.blue  )
		{
			EraseRing(WIN_HDC event_extra);
		
			//PF_ParamDef *color_parcm = params[FILTER_COLOR];
			//PickerPtr pData = PICKER_DATA;
			
			PICKER_DATA->r = params[FILTER_COLOR]->u.cd.value.red;
			PICKER_DATA->g = params[FILTER_COLOR]->u.cd.value.green;
			PICKER_DATA->b = params[FILTER_COLOR]->u.cd.value.blue;
			
			RGBtoHSV(PICKER_DATA->r, PICKER_DATA->g, PICKER_DATA->b,
						&PICKER_DATA->h, &PICKER_DATA->s, &PICKER_DATA->v);
			
			DrawRing(WIN_HDC event_extra);
			
			//UPDATE_SLIDERS( PICKER_DATA );
		}
#endif
		
		if( (PICKER_DATA->which > PICKER_VHS) || (PICKER_DATA->which < PICKER_RGB) )
			PICKER_DATA->which = PICKER_HSV;

		DrawSquare(WIN_HDC NULL, event_extra);
		
		DrawRect(WIN_HDC event_extra);
		
		DrawArrows(WIN_HDC event_extra);
		DrawRing(WIN_HDC event_extra);
		
		DrawTextBoxes(WIN_HDC event_extra);
		DrawTextValues(WIN_HDC event_extra);

		event_extra->evt_out_flags = PF_EO_HANDLED_EVENT;	
	} 

	return err;
}


static PF_Err 
ChangePicker(PF_EventExtra	*event_extra)
{
	PF_Err 			err			= PF_Err_NONE;

	if(CLICK_HIT == CLICK_BOXES)
	{
		if(LITTLE_UI)
			PICKER_DATA->which = (event_extra->u.do_click.screen_point.v - V_OFFSET) / TEXT_LITTLE_STEP;
		else
			PICKER_DATA->which = (event_extra->u.do_click.screen_point.h - H_OFFSET + SQUARE_OFFSET + TEXT_BOX_HSHIFT - (TEXT_LABEL_HSHIFT + TEXT_BOX_HILITE_LEXT) ) / TEXT_STEP;

		PICKER_DATA->which = MIN(PICKER_DATA->which, PICKER_VHS);
		
/*		DrawSquare(WIN_HDC NULL, event_extra);
		
		DrawRect(WIN_HDC event_extra);
		
		DrawArrows(WIN_HDC event_extra);
		DrawRing(WIN_HDC event_extra);
		
		DrawTextBoxes(WIN_HDC event_extra);
		DrawTextValues(WIN_HDC event_extra);
*/	}

	return err;
}


static PF_Point ProperPoint(PF_Point pt, PF_Rect rt)
{
	PF_Point out_pt;

#if WIN_OFFSET
	pt.h++; pt.v++; // fixes annoying windows offset issue
#endif
	
	if( pt.h >= rt.left && pt.h < rt.right )
		out_pt.h = pt.h;
	else if(pt.h < rt.left)
		out_pt.h = rt.left;
	else
		out_pt.h = rt.right - 1;

	if( pt.v >= rt.top && pt.v < rt.bottom )
		out_pt.v = pt.v;
	else if(pt.v < rt.top)
		out_pt.v = rt.top;
	else
		out_pt.v = rt.bottom - 1;

	return out_pt;
}


static PF_Err ProcessSquare(PF_Point point, PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	PF_Pixel square_color;

	//SquareColorValue(&square_color, point.h, point.v, event_extra);
	

	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			SquareColorValue(&square_color, point.h, point.v, event_extra);
			PICKER_DATA->g = square_color.green;
			PICKER_DATA->b = square_color.blue;
			HSVfromRGB(event_extra);
			break;
		
		case PICKER_GBR:
			SquareColorValue(&square_color, point.h, point.v, event_extra);
			PICKER_DATA->r = square_color.red;
			PICKER_DATA->b = square_color.blue;
			HSVfromRGB(event_extra);
			break;

		case PICKER_BRG:
			SquareColorValue(&square_color, point.h, point.v, event_extra);
			PICKER_DATA->g = square_color.green;
			PICKER_DATA->r = square_color.red;
			HSVfromRGB(event_extra);
			break;

		case PICKER_HSV:
			PICKER_DATA->s = 255 - point.h;
			PICKER_DATA->v = 255 - point.v;
			RGBfromHSV(event_extra);
			break;

		case PICKER_SVH:
			PICKER_DATA->v = 255 - point.v;
			PICKER_DATA->h = (unsigned short)((255.0 - (float)point.h) * (360.0 / 255.0));
			RGBfromHSV(event_extra);
			break;

		case PICKER_VHS:
			PICKER_DATA->h = (unsigned short)((255.0 - (float)point.h) * (360.0 / 255.0));
			PICKER_DATA->s = 255 - point.v;
			RGBfromHSV(event_extra);
			break;
	}			
	
	return err;
}


static PF_Err ProcessRect(PF_Point point, PF_EventExtra *event_extra)
{
	PF_Err err = PF_Err_NONE;
	PF_Pixel rect_color;
	
	//RectColorValue(&rect_color, point.h, point.v, event_extra);
	
	switch(PICKER_DATA->which)
	{
		case PICKER_RGB:
			RectColorValue(&rect_color, point.h, point.v, event_extra);
			PICKER_DATA->r = rect_color.red;
			HSVfromRGB(event_extra);
			break;
		
		case PICKER_GBR:
			RectColorValue(&rect_color, point.h, point.v, event_extra);
			PICKER_DATA->g = rect_color.green;
			HSVfromRGB(event_extra);
			break;

		case PICKER_BRG:
			RectColorValue(&rect_color, point.h, point.v, event_extra);
			PICKER_DATA->b = rect_color.blue;
			HSVfromRGB(event_extra);
			break;

		case PICKER_HSV:
			PICKER_DATA->h = (unsigned short)((255.0 - (float)point.v) * (360.0 / 255.0));
			RGBfromHSV(event_extra);
			break;

		case PICKER_SVH:
			PICKER_DATA->s = 255 - point.v;
			RGBfromHSV(event_extra);
			break;

		case PICKER_VHS:
			PICKER_DATA->v = 255 - point.v;
			RGBfromHSV(event_extra);
			break;
	}			
	
	return err;
}


static PF_Err
UIEvent(WIN_HDC_TYPE PF_EventExtra	*event_extra)
{
	PF_Err 			err			= PF_Err_NONE;
	PF_Point		mouse_down, local_pos;
	PF_Rect			dragRect;

	if(CLICK_HIT == CLICK_SQUARE)
	{
		dragRect.top = V_OFFSET + SQUARE_OFFSET;
		dragRect.left = H_OFFSET + SQUARE_OFFSET;
		dragRect.bottom = dragRect.top + LITTLE_HALF(SQUARE_SIZE);
		dragRect.right = dragRect.left + LITTLE_HALF(SQUARE_SIZE);
		
		mouse_down = ProperPoint(event_extra->u.do_click.screen_point, dragRect);
		
		local_pos.h = LITTLE_DOUBLE(mouse_down.h - dragRect.left);
		local_pos.v = LITTLE_DOUBLE(mouse_down.v - dragRect.top);
		
#if !USE_DRAWBOT
		EraseSquareArrowH(WIN_HDC event_extra);
		EraseSquareArrowV(WIN_HDC event_extra);
		EraseRing(WIN_HDC event_extra);
#endif		
		ProcessSquare(local_pos, event_extra);
#if !USE_DRAWBOT		
		DrawRect(WIN_HDC event_extra);
		DrawRing(WIN_HDC event_extra);
		DrawSquareArrowH(WIN_HDC event_extra);
		DrawSquareArrowV(WIN_HDC event_extra);
		DrawTextValues(WIN_HDC event_extra);
		HiliteChanges(WIN_HDC CLICK_HIT, event_extra);
#endif
#if USE_DRAWBOT	
		suites.AppSuite()->PF_InvalidateRect(event_extra->contextH, NULL);
		event_extra->evt_out_flags |= PF_EO_UPDATE_NOW;
#endif
	}
	else if(CLICK_HIT == CLICK_RECT)
	{
		dragRect.top = V_OFFSET + SQUARE_OFFSET;
		dragRect.left = H_OFFSET + LITTLE_HALF(RECT_OFFSET);
		dragRect.bottom = dragRect.top + LITTLE_HALF(SQUARE_SIZE);
		dragRect.right = dragRect.left + LITTLE_HALF(RECT_WIDTH);
		
		mouse_down = ProperPoint(event_extra->u.do_click.screen_point, dragRect);

		local_pos.h = LITTLE_DOUBLE(mouse_down.h - dragRect.left);
		local_pos.v = LITTLE_DOUBLE(mouse_down.v - dragRect.top);
#if !USE_DRAWBOT		
		EraseRectArrow(WIN_HDC event_extra);
#endif		
		ProcessRect(local_pos, event_extra);
#if !USE_DRAWBOT
		DrawRectArrow(WIN_HDC event_extra);
		
		DrawSquare(WIN_HDC NULL, event_extra);
		DrawRing(WIN_HDC event_extra);
		DrawTextValues(WIN_HDC event_extra);
		HiliteChanges(WIN_HDC CLICK_HIT, event_extra);
#endif
#if USE_DRAWBOT	
		suites.AppSuite()->PF_InvalidateRect(event_extra->contextH, NULL);
		event_extra->evt_out_flags |= PF_EO_UPDATE_NOW;
#endif
	}
	else if(CLICK_HIT == CLICK_SQUARE_H_ARROW)
	{				
		dragRect.top = V_OFFSET + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE);
		dragRect.left = H_OFFSET + SQUARE_OFFSET;
		dragRect.bottom = dragRect.top + ARROW_SIZE + 2;
		dragRect.right = dragRect.left + LITTLE_HALF(SQUARE_SIZE);
		
		mouse_down = ProperPoint(event_extra->u.do_click.screen_point, dragRect);
		
		local_pos.h = LITTLE_DOUBLE(mouse_down.h - dragRect.left);
		local_pos.v = Ycoord(event_extra);
#if !USE_DRAWBOT		
		EraseSquareArrowH(WIN_HDC event_extra);
		EraseRing(WIN_HDC event_extra);
#endif		
		ProcessSquare(local_pos, event_extra);
#if !USE_DRAWBOT		
		DrawRect(WIN_HDC event_extra);
		DrawRing(WIN_HDC event_extra);
		DrawSquareArrowH(WIN_HDC event_extra);
		DrawTextValues(WIN_HDC event_extra);
		HiliteChanges(WIN_HDC CLICK_HIT, event_extra);
#endif
#if USE_DRAWBOT	
		suites.AppSuite()->PF_InvalidateRect(event_extra->contextH, NULL);
		event_extra->evt_out_flags |= PF_EO_UPDATE_NOW;
#endif
	}
	else if(CLICK_HIT == CLICK_SQUARE_V_ARROW)
	{				
		dragRect.top = V_OFFSET + SQUARE_OFFSET;
		dragRect.left = H_OFFSET + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE);
		dragRect.bottom = dragRect.top + LITTLE_HALF(SQUARE_SIZE);
		dragRect.right = dragRect.left + ARROW_SIZE + 2;
		
		mouse_down = ProperPoint(event_extra->u.do_click.screen_point, dragRect);
		
		local_pos.h = Xcoord(event_extra);
		local_pos.v = LITTLE_DOUBLE(mouse_down.v - dragRect.top);
#if !USE_DRAWBOT		
		EraseSquareArrowV(WIN_HDC event_extra);
		EraseRing(WIN_HDC event_extra);
#endif		
		ProcessSquare(local_pos, event_extra);
#if !USE_DRAWBOT
		DrawRect(WIN_HDC event_extra);
		DrawRing(WIN_HDC event_extra);
		DrawSquareArrowV(WIN_HDC event_extra);
		DrawTextValues(WIN_HDC event_extra);
		HiliteChanges(WIN_HDC CLICK_HIT, event_extra);
#endif
#if USE_DRAWBOT	
		suites.AppSuite()->PF_InvalidateRect(event_extra->contextH, NULL);
		event_extra->evt_out_flags |= PF_EO_UPDATE_NOW;
#endif
	}
	
	return err;
}


static PF_Err 
DoDrag(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{
	PF_Err 			err			= PF_Err_NONE;
	PF_ContextH		contextH	= event_extra->contextH;

#if USE_DRAWBOT
	AEGP_SuiteHandler suites(in_data->pica_basicP);
	
	DRAWBOT_DrawRef			drawbot_ref = NULL;
	//suites.PFEffectCustomUISuite()->PF_GetDrawingReference(event_extra->contextH, &drawbot_ref);
	
	DRAWBOT_SupplierRef		supplier_ref = NULL;
	DRAWBOT_SurfaceRef		surface_ref = NULL;

	//suites.DBDrawbotSuite()->GetSupplier(drawbot_ref, &supplier_ref);
	//suites.DBDrawbotSuite()->GetSurface(drawbot_ref, &surface_ref);
#else // USE_DRAWBOT
#if MSWindows
	void*			dp = (*(event_extra->contextH))->cgrafptr;
	HDC				ecw_hdc	=	0;
	PF_GET_CGRAF_DATA(dp, PF_CGrafData_HDC, &ecw_hdc);
#endif
#endif // USE_DRAWBOT

	if (PF_Window_EFFECT == (*contextH)->w_type) 
	{
		if (PF_EA_CONTROL == event_extra->effect_win.area) 
		{

			UIEvent(WIN_HDC event_extra);

#if USE_HSV
			if(	PICKER_DATA->h != params[FILTER_HUE]->u.sd.value ||
				PICKER_DATA->s != params[FILTER_SAT]->u.sd.value ||
				PICKER_DATA->v != params[FILTER_VAL]->u.sd.value  )
			{
				UPDATE_HSV_SLIDERS( PICKER_DATA );
			}
#else
			if(	PICKER_DATA->r != params[FILTER_COLOR]->u.cd.value.red ||
				PICKER_DATA->g != params[FILTER_COLOR]->u.cd.value.green ||
				PICKER_DATA->b != params[FILTER_COLOR]->u.cd.value.blue  )
			{
				//PF_ParamDef *color_parcm = params[FILTER_COLOR];
				//PickerPtr pData = PICKER_DATA;
				
				params[FILTER_COLOR]->u.cd.value.red = PICKER_DATA->r;
				params[FILTER_COLOR]->u.cd.value.green = PICKER_DATA->g;
				params[FILTER_COLOR]->u.cd.value.blue = PICKER_DATA->b;
				
				params[FILTER_COLOR]->uu.change_flags	= PF_ChangeFlag_CHANGED_VALUE;
				
				UPDATE_SLIDERS(PICKER_DATA);
			}
#endif
		}
	}

	return err;
}


static PF_Err 
ChangeCursor(	
	PF_InData		*in_data,
	PF_OutData		*out_data,
	PF_ParamDef		*params[],
	PF_LayerDef		*output,
	PF_EventExtra	*event_extra)
{

	PF_ContextH		contextH	= event_extra->contextH;

	if (PF_Window_EFFECT == (*contextH)->w_type) 
	{
		if (PF_EA_CONTROL == event_extra->effect_win.area) 
		{
			PF_Point mouse_down = event_extra->u.adjust_cursor.screen_point;
			PF_Rect ui_area = event_extra->effect_win.current_frame;
#if WIN_OFFSET
			mouse_down.h++; mouse_down.v++; // annoying windows offset issue
#endif			
			if(	mouse_down.h >= (ui_area.left + SQUARE_OFFSET) &&
				mouse_down.h <= (ui_area.left + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) &&
				mouse_down.v >= (ui_area.top  + SQUARE_OFFSET) &&
				mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) )
			{
				event_extra->u.adjust_cursor.set_cursor = PF_Cursor_CROSSHAIRS;
				event_extra->evt_out_flags |= PF_EO_HANDLED_EVENT;
			}
			else if(mouse_down.h >= (ui_area.left + LITTLE_HALF(RECT_OFFSET) ) &&
					mouse_down.h <= (ui_area.left + LITTLE_HALF(RECT_OFFSET) + LITTLE_HALF(RECT_WIDTH) ) &&
					mouse_down.v >= (ui_area.top  + SQUARE_OFFSET) &&
					mouse_down.v <= (ui_area.top  + SQUARE_OFFSET + LITTLE_HALF(SQUARE_SIZE) ) )
			{
				event_extra->u.adjust_cursor.set_cursor = PF_Cursor_SCALE_VERT;
				event_extra->evt_out_flags |= PF_EO_HANDLED_EVENT;
			}
		}
	}

	return PF_Err_NONE;
}
